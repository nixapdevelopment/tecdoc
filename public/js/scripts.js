
$(document).ready(function(){
    
    $('#subscribe-form').submit(function(event){
        event.preventDefault();
        
        var form = $(this);
        
        $.post('/main/subscribe', form.serialize(), function(html){
            form.append(html);
            form.trigger('reset');
        });
    });
    
    $('a.fancybox').fancybox();
    
});


$(document).ready(function() {
	
	var owl = $("#home-slide-1, #home-slide-2");
	
	owl.owlCarousel({
		navigation : 		false, // Show next and prev buttons
		slideSpeed : 		300,
		paginationSpeed : 	400,
		singleItem: 		true,	 
		slideSpeed : 200,
		paginationSpeed : 800, 
		autoPlay : true
	});
 
});