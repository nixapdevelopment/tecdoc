<?php defined('BASEPATH') OR exit('No direct script access allowed');

class User extends UserController
{
    
    public function __construct()
    {
        parent::__construct();
        
        $this->header = 'layouts/user/header';
        $this->footer = 'layouts/user/footer';
        
        $this->breadscrumbs = [];
        $this->addBreadscrumb('user', lang('Dashboard'));
        
        $this->menuType = 'User';
    }

    public function index()
    {
        $this->orders();
    }
    
    public function orders()
    {
        $this->addBreadscrumb('', lang('Orders'));
        
        $this->load->helper('form');
        
        $ClientID = $this->session->userdata('UserID');
        $Date = $this->input->get('Date');
        $PaymentType = $this->input->get('PaymentType');
        $Status = $this->input->get('Status');
        $Page = $this->input->get('page') ? (int)$this->input->get('page') : 1;
        
        $per_page = 10;
        
        $this->db->select('SQL_CALC_FOUND_ROWS *', false);
        $this->db->from('Order as o');
        $this->db->join('OrderDetail as od', 'od.OrderID = o.ID', 'LEFT');
        $this->db->order_by('o.Date', 'DESC');
        $this->db->where('o.UserID', $ClientID);
        if (!empty($Date)) $this->db->where("DATE_FORMAT(o.Date, '%Y-%m-%d') =", date('Y-m-d', strtotime($Date)));
        if (!empty($PaymentType))$this->db->where('o.PaymentType', $PaymentType);
        if (!empty($Status))$this->db->where('o.Status', $Status);
        $this->db->limit($per_page);
        $this->db->offset(($Page - 1) * $per_page);
        $data['orders'] = $this->db->get()->result();
        
        $total_orders = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;
        
        $this->load->library('pagination');
        $config['base_url'] = site_url('user/orders', [], true);
        $config['total_rows'] = $total_orders;
        $config['per_page'] = $per_page;
        $config['use_page_numbers'] = true;
        $config['page_query_string'] = true;
        $config['query_string_segment'] = 'page';
        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] ="</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tag_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tag_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tag_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tag_close'] = "</li>";
        $config['num_links'] = 4;
        $get = $this->input->get();
        unset($get['page']);
        if (count($this->input->get()) > 0) $config['suffix'] = '&' . http_build_query($get);
        $config['first_url'] = $config['base_url']. '?' . http_build_query($get);
        $this->pagination->initialize($config); 
        $data['pagination'] = $this->pagination->create_links();
        
        $this->render('user/orders', $data);
    }
    
    public function order($order_id = false)
    {
        if (!$order_id) redirect('user/orders');
        
        $this->db->select('*');
        $this->db->from('Order as o');
        $this->db->join('OrderDetail as od', 'od.OrderID = o.ID', 'LEFT');
        $this->db->where('o.ID', $order_id);
        $this->db->where('o.UserID', $this->session->userdata('UserID'));
        $data['order'] = $this->db->get()->row();
        
        if (empty($data['order'])) redirect('user/orders');
        
        $this->config->load('regions');
        
        $this->addBreadscrumb(site_url('user/orders'), lang('Orders'));
        $this->addBreadscrumb('', '# ' . $data['order']->OrderID);
        
        $data['client'] = $this->db->get_where('User', ['ID' => $data['order']->UserID], 1)->row();
        
        $this->db->select('*, op.Price');
        $this->db->from('OrderProduct as op');
        $this->db->join('Product as p', 'p.ID = op.ProductID');
        $this->db->join('ProductLang as pl', 'pl.ProductID = p.ID AND pl.LangID = ' . $this->langID);
        $this->db->where('op.OrderID', $data['order']->OrderID);
        $data['order_products'] = $this->db->get()->result();
        
        $this->render('user/order', $data);
    }
    
}