<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends AdminController
{
    
    public $menuType = 'Admin';
    
    public function index()
    {
        $data['product_count'] = $this->db->where('Status <>', 'Deleted')->get('Product')->num_rows();
        $data['category_count'] = $this->db->where('Status <>', 'Deleted')->get('Category')->num_rows();
        $data['client_count'] = $this->db->where('Status <>', 'Deleted')->where_in('Type', ['Angro', 'Retail'])->get('User')->num_rows();
        
        $this->db->select('*');
        $this->db->from('Order as o');
        $this->db->join('OrderDetail as od', 'od.OrderID = o.ID');
        $this->db->order_by('o.Date', 'DESC');
        $this->db->limit(10);
        $data['orders'] = $this->db->get()->result();
        
        $this->render('admin/index', $data);
    }
    
    public function categories()
    {
        $nodeList = array();
        $tree     = array();

        $categories = $this->db->simple_query("select c.ID, c.ParentID, cl.Name from `Category` as c left join `CategoryLang` as cl on cl.LangID = " . $this->langID . " and c.ID = cl.CategoryID");
        
        foreach ($categories as $row)
        {
            $nodeList[$row['ID']] = array_merge($row, array('children' => array()));
        }

        foreach ($nodeList as $nodeId => &$node)
        {
            if (!$node['ParentID'] || !array_key_exists($node['ParentID'], $nodeList))
            {
                $tree[] = &$node;
            }
            else
            {
                $nodeList[$node['ParentID']]['children'][] = &$node;
            }
        }
        unset($node);
        unset($nodeList);
        
        $tree = $this->draw_tree($tree);
        
        if ($this->input->is_ajax_request())
        {
            exit($tree);
        }
        
        $this->render('admin/categories', [
            'tree' => $tree
        ]);
    }

    private function draw_tree($tree)
    {
        $return = '<ul>';
        foreach ($tree as $row)
        {
            $return .= '<li cat-id="' . $row['ID'] . '">';
            $return .= $row['Name'];
            if (!empty($row['children']))
            {
                $return .= $this->draw_tree($row['children']);
            }
            $return .= '</li>';
        }
        $return .= '</ul>';
        return $return;
    }

    public function get_category_form()
    {
        $this->load->helper('form');
        
        $categoryID = (int)$this->input->post('categoryID');
        $parentID = (int)$this->input->post('parentID');
        
        $data = [];
        $data['parentID'] = $parentID;
        $data['categoryID'] = $categoryID;
        $data['category_link'] = '';
        
        if ($categoryID > 0)
        {
            // get category
            $this->db->select('*');
            $this->db->from('Category');
            $this->db->where('ID', $categoryID);
            $this->db->where('Status !=', 'Deleted');
            $data['category'] = $this->db->get()->row();
            
            // get category langs
            $this->db->select('*');
            $this->db->from('CategoryLang');
            $this->db->where('CategoryID', $categoryID);
            $res = $this->db->get()->result();
            
            $data['category_langs'] = [];
            foreach ($res as $row)
            {
                $data['category_langs'][$row->LangID] = $row;
            }
            
            // get category url
            $url = $this->db->get_where('Url', ['Type' => 'Category', 'ObjectID' => $categoryID], 1)->row();
            $data['category_link'] = isset($url->Link) ? $url->Link : '';
        }
        
        // get all categories
        $this->db->select('c.ID, cl.Name');
        $this->db->from('Category as c');
        $this->db->join('CategoryLang as cl', 'cl.CategoryID = c.ID and cl.LangID = ' . $this->langID);
        $this->db->where('Status', 'Active');
        $this->db->order_by('cl.Name');
        $res = $this->db->get();

        $data['all_categories'] = [];
        $data['all_categories'][0] = '-';
        foreach ($res->result() as $row)
        {
            $data['all_categories'][$row->ID] = $row->Name;
        }

        if ($categoryID > 0)
        {
            unset($data['all_categories'][$categoryID]);
        }
        
        echo $this->load->view('admin/category_form', $data, true);
    }
    
    public function save_category()
    {
        // get data
        $categoryID = $this->input->post('CategoryID');
        $parentID = $this->input->post('ParentID');
        $names = $this->input->post('Name');
        $titles = $this->input->post('Title');
        $keywords = $this->input->post('Keywords');
        $descriptions = $this->input->post('Description');
        $link = $this->input->post('Link');
        $status = $this->input->post('Status') == 'Disabled' ? 'Disabled' : 'Active';
        $type = $this->input->post('Type');
        
        // save category
        $category = [
            'ParentID' => $parentID,
            'Status' => $status,
            'Type' => $type
        ];
        
        // upload category image
        $config['upload_path'] = 'public/uploads/categories';
        $config['allowed_types'] = 'jpg|png';
        $config['max_size']	= '10000';
        $config['max_width']  = '4000';
        $config['max_height']  = '4000';
        $config['encrypt_name']  = true;
        $this->load->library('upload', $config);
        
        if ($this->upload->do_upload('Image'))
        {
            $file = $this->upload->data();
            $category['Image'] = $file['file_name'];
        }
        
        if ($categoryID > 0)
        {
            $this->db->update('Category', $category, ['ID' => $categoryID]);
        }
        else
        {
            $this->db->insert('Category', $category);
            $categoryID = $this->db->insert_id();
        }
        
        // save url
        if (empty($link))
        {
            $link = str_to_url($names[1]);
        }
        
        $this->db->select('*');
        $this->db->from('Url');
        $res = $this->db->get()->result_array();
        
        $all_urls = [];
        $all_urls_ot = [];
        foreach ($res as $row)
        {
            $all_urls[$row['Link']] = $row;
            $all_urls_ot[$row['Type']][$row['ObjectID']] = $row;
        }
        
        if (isset($all_urls[$link]))
        {
            if ($all_urls[$link]['ObjectID'] != $categoryID || $all_urls[$link]['Type'] != 'Category')
            {
                $i = 1;
                $str = $link;
                $links = array_keys($all_urls);
                while (in_array($str, $links))
                {
                    $str .= '-' . $i;
                    $i++;
                }
                $link .= '-' . $i;
            }
        }
        
        if (isset($all_urls_ot['Category'][$categoryID]))
        {
            $this->db->update('Url', ['Link' => $link], ['ID' => $all_urls_ot['Category'][$categoryID]['ID']]);
        }
        else
        {
            $this->db->insert('Url', [
                'Link' => $link,
                'ObjectID' => $categoryID,
                'Type' => 'Category'
            ]);
        }
        
        // save category langs
        $this->db->delete('CategoryLang', ['CategoryID' => $categoryID]);
        
        $insert = [];
        foreach ($names as $langID => $name)
        {
            $insert[] = [
                'CategoryID' => $categoryID,
                'LangID' => $langID,
                'Name' => $names[$langID],
                'Title' => $titles[$langID],
                'Keywords' => $keywords[$langID],
                'Description' => $descriptions[$langID]
            ];
        }
        $this->db->insert_batch('CategoryLang', $insert);
        
        exit(json_encode([
            'categoryId' => $categoryID,
            'ParentID' => $parentID
        ]));
    }

    public function delete_category()
    {
        $categoryID = (int)$this->input->post('categoryID');
        
        $this->db->delete('Category', [
            'ID' => $categoryID
        ]);
        
        $this->db->delete('CategoryLang', [
            'CategoryID' => $categoryID
        ]);
        
        $this->db->delete('Url', [
            'ObjectID' => $categoryID,
            'Type' => 'Category'
        ], 1);
        
        $this->delete_subcategories($categoryID);
    }
    
    private function delete_subcategories($parentID)
    {
        $subcategories = $this->db->get_where('Category', ['ParentID' => $parentID])->result();
        
        $this->db->delete('Category', [
            'ParentID' => $parentID
        ]);
        
        if (count($subcategories) > 0)
        {
            foreach ($subcategories as $subcat)
            {
                $this->delete_subcategories($subcat->ID);
                $this->db->delete('CategoryLang', [
                    'CategoryID' => $subcat->ID
                ]);
                $this->db->delete('Url', [
                    'ObjectID' => $subcat->ID,
                    'Type' => 'Category'
                ], 1);
            }
        }
    }
    
    public function filters()
    {
        $this->load->helper('form');
        
        if (isset($_GET['delId']))
        {
            $this->db->delete('Filter', ['ID' => (int)$_GET['delId']]);
            $this->db->delete('FilterLang', ['FilterID' => (int)$_GET['delId']]);
            
            $this->session->set_flashdata('success', lang('FilterDeleted'));
            
            redirect('admin/filters');
        }
        
        if (count($_POST) > 0)
        {
            $names = $this->input->post('Name');
            $type = $this->input->post('Type');
            $filterID = (int)$this->input->post('FilterID');
            
            if ($filterID > 0)
            {
                $this->db->update('Filter', ['Type' => $type], ['ID' => $filterID]);
                $this->db->delete('FilterLang', ['FilterID' => $filterID]);
            }
            else
            {
                $this->db->insert('Filter', [
                    'Type' => $type
                ]);
                $filterID = $this->db->insert_id();
            }
            
            $insert = [];
            foreach ($names as $langID => $name)
            {
                $insert[] = [
                    'FilterID' => $filterID,
                    'LangID' => $langID,
                    'Name' => $name
                ];
            }
            $this->db->insert_batch('FilterLang', $insert);
            
            $this->session->set_flashdata('success', lang('FilterSaved'));
            
            redirect('admin/filters');
        }
        
        $this->db->select('*');
        $this->db->from('Filter as f');
        $this->db->join('FilterLang as fl', 'fl.FilterID = f.ID');
        $this->db->order_by('f.ID');
        $res = $this->db->get()->result();
        
        $data['filters'] = [];
        foreach ($res as $row)
        {
            $data['filters'][$row->ID]['Name'][$row->LangID] = $row->Name;
            $data['filters'][$row->ID]['Type'] = $row->Type;
            $data['filters'][$row->ID]['System'] = $row->System;
        }
        
        if (!empty($_GET['id']))
        {
            $id = (int)$_GET['id'];
            $data['filter'] = $data['filters'][$id];
        }
        
        $this->render('admin/filters', $data);
    }
    
    public function products()
    {
        $this->load->helper('form');
        
        // get all categories
        $this->db->select('c.ID, cl.Name');
        $this->db->from('Category as c');
        $this->db->join('CategoryLang as cl', 'cl.CategoryID = c.ID and cl.LangID = ' . $this->langID);
        $this->db->where('Status', 'Active');
        $this->db->order_by('cl.Name');
        $res = $this->db->get();

        $data['all_categories'] = [];
        $data['all_categories'][0] = '-';
        foreach ($res->result() as $row)
        {
            $data['all_categories'][$row->ID] = $row->Name;
        }
        
        $page = $this->input->get('page');
        $categories = $this->input->get('Category[]');
        $name = $this->input->get('Name');
        $sort = $this->input->get('Sort');
        $is_promo = $this->input->get('IsPromo');
        $sku = $this->input->get('Sku');
        
        $this->db->select('*, IF(IsPromo = 1, DiscountPrice, Price) as ProductPrice');
        $this->db->from('Product as p');
        $this->db->join('ProductLang as pl', 'pl.ProductID = p.ID and pl.LangID = ' . $this->langID, 'LEFT');
        
        if (!empty($categories))
        {
            $this->db->where_in('p.CategoryID', $categories);
        }
        
        if (!empty($name))
        {
            $this->db->like('pl.Name', $name);
        }
        
        if (!empty($sort))
        {
            switch ($sort)
            {
                case 'az':
                    $this->db->order_by('pl.Name asc');
                    break;
                case 'za':
                    $this->db->order_by('pl.Name desc');
                    break;
                case 'price_d':
                    $this->db->order_by('ProductPrice desc');
                    break;
                case 'price_a':
                    $this->db->order_by('ProductPrice asc');
                    break;
                default:
                    $this->db->order_by('ID asc');
                    break;
            }
        }
        
        if (!empty($is_promo))
        {
            $this->db->where('IsPromo', 1);
        }
        
        if (!empty($sku))
        {
            $this->db->like('Sku', $sku, 'after');
        }
        
        if (!empty($page))
        {
            $this->db->offset($page * 10);
        }
        
        $this->db->limit(10);
        $data['products'] = $this->db->get()->result();
        
        $this->render('admin/products', $data);
    }
    
    public function edit_product()
    {
        $id = (int)$this->input->get('id');
        
        $this->load->helper('form');
        
        if (count($_POST) > 0)
        {
            $category_id = $this->input->post('CategoryID');
            $link = $this->input->post('Link');
            $sku = $this->input->post('Sku');
            $status = $this->input->post('Status');
            $type = $this->input->post('Type');
            $names = $this->input->post('Name');
            $keywords = $this->input->post('Keywords');
            $descriptions = $this->input->post('Description');
            $price = $this->input->post('Price');
            $stock = $this->input->post('Stock');
            $is_promo = $this->input->post('IsPromo') == 1 ? 1 : 0;
            $discount_price = $this->input->post('DiscountPrice');
            $discount_price_start = $this->input->post('DiscountPriceStart');
            $discount_price_end = $this->input->post('DiscountPriceEnd');
            $primary_image = $this->input->post('PrimaryImage');
            $filter_values = $this->input->post('filter_value');
            
            if (empty($link))
            {
                $link = str_to_url($names[1]);
            }
            
            // save product
            $product = [
                'CategoryID' => $category_id,
                'Sku' => $sku,
                'Stock' => $stock,
                'Price' => $price,
                'DiscountPrice' => $discount_price,
                'DiscountPriceStart' => date('c', strtotime($discount_price_start)),
                'DiscountPriceEnd' => date('c', strtotime($discount_price_end)),
                'IsPromo' => $is_promo,
                'Type' => $type,
                'Status' => $status
            ];
            
            if ($id > 0)
            {
                $this->db->update('Product', $product, ['ID' => $id]);
            }
            else
            {
                $this->db->insert('Product', $product);
                $id = $this->db->insert_id();
            }
            
            // save product langs
            $this->db->delete('ProductLang', ['ProductID' => $id]);
            
            $insert = [];
            foreach ($names as $langID => $name)
            {
                $insert[] = [
                    'ProductID' => $id,
                    'LangID' => $langID,
                    'Name' => $names[$langID],
                    'Keywords' => $keywords[$langID],
                    'Description' => $descriptions[$langID]
                ];
            }
            $this->db->insert_batch('ProductLang', $insert);
            
            // save link
            $this->db->select('*');
            $this->db->from('Url');
            $res = $this->db->get()->result_array();

            $all_urls = [];
            $all_urls_ot = [];
            foreach ($res as $row)
            {
                $all_urls[$row['Link']] = $row;
                $all_urls_ot[$row['Type']][$row['ObjectID']] = $row;
            }

            if (isset($all_urls[$link]))
            {
                if ($all_urls[$link]['ObjectID'] != $id || $all_urls[$link]['Type'] != 'Product')
                {
                    $i = 1;
                    $str = $link;
                    $links = array_keys($all_urls);
                    while (in_array($str, $links))
                    {
                        $str .= '-' . $i;
                        $i++;
                    }
                    $link .= '-' . $id;
                }
            }

            if (isset($all_urls_ot['Product'][$id]))
            {
                $this->db->update('Url', ['Link' => $link], ['ID' => $all_urls_ot['Product'][$id]['ID']]);
            }
            else
            {
                $this->db->insert('Url', [
                    'Link' => $link,
                    'ObjectID' => $id,
                    'Type' => 'Product'
                ]);
            }
            
            // save filters
            $this->db->delete('ProductFilter', ['ProductID' => $id]);
            
            if (!empty($filter_values))
            {
                $insert = [];
                foreach ($filter_values as $filterID => $filterData)
                {
                    $this->db->insert('ProductFilter', [
                        'ProductID' => $id,
                        'FilterID' => $filterID
                    ]);
                    $productFilterID = $this->db->insert_id();
                    foreach ($this->config->item('languages') as $langID => $lang)
                    {
                        $insert[] = [
                            'ProductFilterID' => $productFilterID,
                            'LangID' => $langID,
                            'Value' => !empty($filter_values[$filterID][$langID]) ? $filter_values[$filterID][$langID] : $filter_values[$filterID][1]
                        ];
                    }
                }
                $this->db->insert_batch('ProductFilterLang', $insert);
            }
            
            // mark primary image
            if (!empty($primary_image))
            {
                $this->db->update('ProductImage', ['IsMain' => 0], ['ProductID' => $id]);
                $this->db->update('ProductImage', ['IsMain' => 1], ['ID' => $primary_image], 1);
            }
            
            // save images
            $config = [
                'upload_path'   => 'public/uploads/products',
                'allowed_types' => 'jpg|png',
                'encrypt_name'  => true
            ];
            $this->load->library('upload', $config);

            if (!empty($_FILES['Images']['name']))
            {
                $images = [];
                foreach ($_FILES['Images']['name'] as $key => $image)
                {
                    $_FILES['images']['name']= $_FILES['Images']['name'][$key];
                    $_FILES['images']['type']= $_FILES['Images']['type'][$key];
                    $_FILES['images']['tmp_name']= $_FILES['Images']['tmp_name'][$key];
                    $_FILES['images']['error']= $_FILES['Images']['error'][$key];
                    $_FILES['images']['size']= $_FILES['Images']['size'][$key];

                    $this->upload->initialize($config);

                    if ($this->upload->do_upload('images'))
                    {
                        $images[] = $this->upload->data();
                    }
                }

                $config_manip = [
                    'image_library' => 'gd2',
                    'maintain_ratio' => TRUE,
                ];
                $this->load->library('image_lib', $config);

                $insert = [];
                foreach ($images as $img)
                {
                    $config_manip['source_image'] = 'public/uploads/products/' . $img['file_name'];
                    $config_manip['new_image'] = 'public/uploads/products/thumb_' . $img['file_name'];
                    $config_manip['width'] = 300;
                    $config_manip['height'] = 300;

                    $this->image_lib->initialize($config_manip);
                    $this->image_lib->resize();
                    $this->image_lib->clear();

                    $config_manip['source_image'] = 'public/uploads/products/' . $img['file_name'];
                    $config_manip['new_image'] = 'public/uploads/products/' . $img['file_name'];
                    $config_manip['width'] = 1200;
                    $config_manip['height'] = 1200;

                    $this->image_lib->initialize($config_manip);
                    $this->image_lib->resize();
                    $this->image_lib->clear();

                    $insert[] = [
                        'ProductID' => $id,
                        'Image' => $img['file_name'],
                        'Thumb' => 'thumb_' . $img['file_name'],
                        'IsMain' => empty($primary_image) ? 1 : 0
                    ];

                    $primary_image = true;
                }
                
                if (count($insert) > 0)
                {
                    $this->db->insert_batch('ProductImage', $insert);
                }
            }
            
            $this->session->set_flashdata('success', '<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' . lang('ProductSaved') . '</div>');
                
            $red = site_url('admin/edit_product', ['id' => $id]);
            redirect($red);
        }
        
        // get all categories
        $this->db->select('c.ID, cl.Name');
        $this->db->from('Category as c');
        $this->db->join('CategoryLang as cl', 'cl.CategoryID = c.ID and cl.LangID = ' . $this->langID);
        $this->db->where('Status', 'Active');
        $this->db->order_by('cl.Name');
        $res = $this->db->get();

        $data['all_categories'] = [];
        $data['all_categories'][''] = '-';
        foreach ($res->result() as $row)
        {
            $data['all_categories'][$row->ID] = $row->Name;
        }
        
        // get all filters
        $this->db->select('f.ID, fl.Name');
        $this->db->from('Filter as f');
        $this->db->join('FilterLang as fl', 'fl.FilterID = f.ID and fl.LangID = ' . $this->langID);
        $this->db->where_in('f.Type', ['Number', 'String']);
        $this->db->order_by('fl.Name');
        $res = $this->db->get();
        
        $data['all_filters'] = [];
        foreach ($res->result() as $row)
        {
            $data['all_filters'][$row->ID] = $row->Name;
        }
        
        $data['product'] = [];
        $data['images'] = [];
        $data['filters'] = [];
        $data['product_filters'] = [];
        $data['product_filter_values'] = [];
        $data['product_link'] = '';
        
        if ($id > 0)
        {
            // get product
            $this->db->select('*');
            $this->db->from('Product');
            $this->db->where('ID', $id);
            $this->db->where('Status !=', 'Deleted');
            $data['product'] = $this->db->get()->row();
            
            // get product langs
            $this->db->select('*');
            $this->db->from('ProductLang');
            $this->db->where('ProductID', $id);
            $res = $this->db->get()->result();
            
            $data['product_langs'] = [];
            foreach ($res as $row)
            {
                $data['product_langs'][$row->LangID] = $row;
            }
            
            // get product url
            $url = $this->db->get_where('Url', ['Type' => 'Product', 'ObjectID' => $id], 1)->row();
            $data['product_link'] = isset($url->Link) ? $url->Link : '';
            
            // get images
            $this->db->select('*');
            $this->db->from('ProductImage');
            $this->db->where('ProductID', $id);
            $this->db->order_by('IsMain', 'DESC');
            $data['images'] = $this->db->get()->result_array();
            
            // get product filters
            $this->db->select('f.ID, pfl.LangID, pfl.Value');
            $this->db->from('ProductFilter as pf');
            $this->db->join('ProductFilterLang as pfl', 'pfl.ProductFilterID = pf.ID');
            $this->db->join('Filter as f', 'f.ID = pf.FilterID');
            $this->db->join('FilterLang as fl', 'f.ID = fl.FilterID and fl.LangID = ' . $this->langID);
            $this->db->where('pf.ProductID', $id);
            $this->db->order_by('fl.Name');
            $res = $this->db->get();

            foreach ($res->result() as $row)
            {
                $data['product_filters'][$row->ID][$row->LangID] = $row->Value;
            }
            
            // get filter values
            if (count($data['product_filters']) > 0)
            {
                $this->db->select('pfl.Value, pfl.LangID, pf.FilterID');
                $this->db->from('ProductFilter as pf');
                $this->db->join('ProductFilterLang as pfl', 'pfl.ProductFilterID = pf.ID');
                $res = $this->db->get()->result();
                
                foreach ($res as $row)
                {
                    $data['product_filter_values'][$row->FilterID][$row->LangID][$row->Value] = $row->Value;
                }
            }
        }
        
        $this->render('admin/edit_product', $data);
    }
    
    public function product_filter_select()
    {
        $filter_id = $this->input->post('id');
        
        $filter = $this->db->get_where('Filter', ['ID' => $filter_id], 1)->row();
        
        $this->db->select('pfl.LangID, pfl.Value');
        $this->db->from('ProductFilter as pf');
        $this->db->join('ProductFilterLang as pfl', 'pfl.ProductFilterID = pf.ID');
        $this->db->where('pf.FilterID', $filter_id);
        $res = $this->db->get()->result();
        
        $values = [];
        foreach ($res as $row)
        {
            $values[$row->LangID][] = $row->Value;
        }
        
        $str = "";
        foreach ($this->config->item('languages') as $langID => $lang)
        {
            $str .= " <div class=\"col-md-2 filter-value-wrap\">"
                     . "<div class=\"input-group\">
                            <span class=\"input-group-addon\">" . $lang['Slug'] . "</span>
                            <select lang-id=\"$langID\" " . ($filter->Type == 'Number' && $langID != 1 ? 'disabled' : '') . " id=\"filter-select-$filter_id-$langID\" type=\"text\" style=\"width: 100%;\" class=\"form-control filter-value-select\" multiple name=\"filter_value[$filter_id][$langID]\" placeholder=\"Value\">
                    ";
            
            if (isset($values[$langID]))
            {
                foreach ($values[$langID] as $val)
                {
                    $str .= "<option value=\"$val\">$val</option>";
                }
            }
            
            $str .= "
                            </select>
                        </div>"
                    . "</div>";
        }
        
        echo $str;
    }

    public function delete_product_image()
    {
        $imageID = (int)$this->input->post('imgID');
        
        if ($imageID > 0)
        {
            $this->db->delete('ProductImage', ['ID' => $imageID]);
        }
    }
    
    public function delete_news_image()
    {
        $imageID = (int)$this->input->post('imgID');
        
        if ($imageID > 0)
        {
            $this->db->delete('NewsImage', ['ID' => $imageID]);
        }
    }
    
    public function delete_page_image()
    {
        $imageID = (int)$this->input->post('imgID');
        
        if ($imageID > 0)
        {
            $this->db->delete('PageImage', ['ID' => $imageID]);
        }
    }
    
    public function filter_search()
    {
        $name = $this->input->get('q');
        
        exit(json_encode([
            [
                'id' => 20,
                'text' => 20
            ]
        ]));
    }
    
    public function news()
    {
        $offset = $this->input->get('page') ? ($this->input->get('page') - 1) * 10 : 0;
        
        $this->load->library('pagination');

        $config['base_url'] = site_url('admin/news');
        $config['total_rows'] = $this->db->count_all_results('News');
        $config['per_page'] = 10;
        $config['use_page_numbers'] = true;
        $config['page_query_string'] = true;
        $config['query_string_segment'] = 'page';
        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] ="</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tag_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tag_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tag_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tag_close'] = "</li>";
        $config['num_links'] = 4;
        $this->pagination->initialize($config); 
        $data['pagination'] = $this->pagination->create_links();
        
        $this->db->select('n.ID, nl.Title, nl.Text, n.Date, n.Status, u.Link');
        $this->db->from('News as n');
        $this->db->join('NewsLang as nl', 'nl.NewsID = n.ID and nl.LangID = ' . $this->langID, 'LEFT');
        $this->db->join('Url as u', "u.ObjectID = n.ID and u.Type = 'News'", 'LEFT');
        $this->db->order_by('n.Date', 'DESC');
        $this->db->offset($offset);
        $this->db->limit(10);
        $data['news'] = $this->db->get()->result();
        
        $this->render('admin/news', $data);
    }
    
    public function edit_news()
    {
        $id = (int)$this->input->get('id');
        
        if (count($_POST) > 0)
        {
            $link = $this->input->post('Link');
            $status = $this->input->post('Status');
            $titles = $this->input->post('Title');
            $texts = $this->input->post('Text');
            $keywords = $this->input->post('Keywords');
            $descriptions = $this->input->post('Description');
            $primary_image = $this->input->post('PrimaryImage');
            $date = $this->input->post('Date');
            
            if (empty($link))
            {
                $link = str_to_url($titles[1]);
            }
            
            // save product
            $article = [
                'Date' => date('c', strtotime($date)),
                'Status' => $status
            ];
            
            if ($id > 0)
            {
                $this->db->update('News', $article, ['ID' => $id]);
            }
            else
            {
                $this->db->insert('News', $article);
                $id = $this->db->insert_id();
            }
            
            // save product langs
            $this->db->delete('NewsLang', ['NewsID' => $id]);
            
            $insert = [];
            foreach ($titles as $langID => $name)
            {
                $insert[] = [
                    'NewsID' => $id,
                    'LangID' => $langID,
                    'Title' => $titles[$langID],
                    'Text' => $texts[$langID],
                    'Keywords' => $keywords[$langID],
                    'Description' => $descriptions[$langID]
                ];
            }
            $this->db->insert_batch('NewsLang', $insert);
            
            // save link
            $this->db->select('*');
            $this->db->from('Url');
            $res = $this->db->get()->result_array();

            $all_urls = [];
            $all_urls_ot = [];
            foreach ($res as $row)
            {
                $all_urls[$row['Link']] = $row;
                $all_urls_ot[$row['Type']][$row['ObjectID']] = $row;
            }

            if (isset($all_urls[$link]))
            {
                if ($all_urls[$link]['ObjectID'] != $id || $all_urls[$link]['Type'] != 'News')
                {
                    $i = 1;
                    $str = $link;
                    $links = array_keys($all_urls);
                    while (in_array($str, $links))
                    {
                        $str .= '-' . $i;
                        $i++;
                    }
                    $link .= '-' . $i;
                }
            }

            if (isset($all_urls_ot['News'][$id]))
            {
                $this->db->update('Url', ['Link' => $link], ['ID' => $all_urls_ot['News'][$id]['ID']]);
            }
            else
            {
                $this->db->insert('Url', [
                    'Link' => $link,
                    'ObjectID' => $id,
                    'Type' => 'News'
                ]);
            }
            
            // mark primary image
            if (!empty($primary_image))
            {
                $this->db->update('NewsImage', ['IsMain' => 0], ['NewsID' => $id]);
                $this->db->update('NewsImage', ['IsMain' => 1], ['ID' => $primary_image], 1);
            }
            
            // save images
            $config = [
                'upload_path'   => 'public/uploads/news',
                'allowed_types' => 'jpg|png',
                'encrypt_name'  => true
            ];
            $this->load->library('upload', $config);

            if (!empty($_FILES['Images']['name']))
            {
                $images = [];
                foreach ($_FILES['Images']['name'] as $key => $image)
                {
                    $_FILES['images']['name']= $_FILES['Images']['name'][$key];
                    $_FILES['images']['type']= $_FILES['Images']['type'][$key];
                    $_FILES['images']['tmp_name']= $_FILES['Images']['tmp_name'][$key];
                    $_FILES['images']['error']= $_FILES['Images']['error'][$key];
                    $_FILES['images']['size']= $_FILES['Images']['size'][$key];

                    $this->upload->initialize($config);

                    if ($this->upload->do_upload('images'))
                    {
                        $images[] = $this->upload->data();
                    }
                }

                $config_manip = [
                    'image_library' => 'gd2',
                    'maintain_ratio' => TRUE,
                ];
                $this->load->library('image_lib', $config);

                $insert = [];
                foreach ($images as $img)
                {
                    $config_manip['source_image'] = 'public/uploads/news/' . $img['file_name'];
                    $config_manip['new_image'] = 'public/uploads/news/thumb_' . $img['file_name'];
                    $config_manip['width'] = 300;
                    $config_manip['height'] = 300;

                    $this->image_lib->initialize($config_manip);
                    $this->image_lib->resize();
                    $this->image_lib->clear();

                    $config_manip['source_image'] = 'public/uploads/news/' . $img['file_name'];
                    $config_manip['new_image'] = 'public/uploads/news/' . $img['file_name'];
                    $config_manip['width'] = 1200;
                    $config_manip['height'] = 1200;

                    $this->image_lib->initialize($config_manip);
                    $this->image_lib->resize();
                    $this->image_lib->clear();

                    $insert[] = [
                        'NewsID' => $id,
                        'Image' => $img['file_name'],
                        'Thumb' => 'thumb_' . $img['file_name'],
                        'IsMain' => empty($primary_image) ? 1 : 0
                    ];

                    $primary_image = true;
                }
                
                if (count($insert) > 0)
                {
                    $this->db->insert_batch('NewsImage', $insert);
                }
            }
            
            if (!empty($_FILES['Files']['name']))
            {
                 $config = [
                    'upload_path'   => 'public/uploads/news',
                    'allowed_types' => '*',
                    'encrypt_name'  => true
                ];
                $this->load->library('upload', $config);
                
                $insert = [];
                foreach ($_FILES['Files']['name'] as $key => $image)
                {
                    $_FILES['files']['name']= $_FILES['Files']['name'][$key];
                    $_FILES['files']['type']= $_FILES['Files']['type'][$key];
                    $_FILES['files']['tmp_name']= $_FILES['Files']['tmp_name'][$key];
                    $_FILES['files']['error']= $_FILES['Files']['error'][$key];
                    $_FILES['files']['size']= $_FILES['Files']['size'][$key];

                    $this->upload->initialize($config);

                    if ($this->upload->do_upload('files'))
                    {
                        $file = $this->upload->data();
                        $insert[] = [
                            'EntityID' => $id,
                            'EntityType' => 'News',
                            'Path' => $file['file_name'],
                            'Name' => $file['client_name']
                        ];
                    }
                }
                
                if (count($insert) > 0)
                {
                    $this->db->insert_batch('File', $insert);
                }
            }
            
            $this->session->set_flashdata('success', '<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' . lang('NewsSaved') . '</div>');
                
            $red = site_url('admin/edit_news', ['id' => $id]);
            redirect($red);
        }
        
        $data['images'] = [];
        $data['files'] = [];
        if ($id > 0)
        {
            // get article
            $this->db->select('*');
            $this->db->from('News');
            $this->db->where('ID', $id);
            $this->db->where('Status !=', 'Deleted');
            $this->db->limit(1);
            $data['article'] = $this->db->get()->row();
            
            // get article langs
            $this->db->select('*');
            $this->db->from('NewsLang');
            $this->db->where('NewsID', $id);
            $res = $this->db->get()->result();
            
            $data['article_langs'] = [];
            foreach ($res as $row)
            {
                $data['article_langs'][$row->LangID] = $row;
            }
            
            // get article url
            $url = $this->db->get_where('Url', ['Type' => 'News', 'ObjectID' => $id], 1)->row();
            $data['article_link'] = isset($url->Link) ? $url->Link : '';
            
            // get article images
            $this->db->select('*');
            $this->db->from('NewsImage');
            $this->db->where('NewsID', $id);
            $this->db->order_by('IsMain', 'DESC');
            $data['images'] = $this->db->get()->result_array();
            
            // get article files
            $this->db->select('*');
            $this->db->from('File');
            $this->db->where('EntityID', $id);
            $this->db->where('EntityType', 'News');
            $data['files'] = $this->db->get()->result();
        }
        
        $this->render('admin/edit_news', $data);
    }
    
    public function delete_news()
    {
        $id = (int)$this->input->get('id');
        $page = (int)$this->input->get('page');
        
        if ($id > 0)
        {
            $this->db->delete('News', ['ID' => $id]);
            $this->db->delete('NewsLang', ['NewsID' => $id]);
            $this->db->delete('File', ['EntityID' => $id, 'EntityType' => 'News']);
            $this->db->delete('NewsImage', ['NewsID' => $id]);
            $this->db->delete('Url', ['ObjectID' => $id, 'Type' => 'News'], 1);
        }
        
        redirect(site_url('admin/news'), $page > 0 ? ['page' => $page] : []);
    }
    
    public function pages()
    {
        $offset = $this->input->get('page') ? ($this->input->get('page') - 1) * 10 : 0;
        
        $this->load->library('pagination');

        $config['base_url'] = site_url('admin/pages');
        $config['total_rows'] = $this->db->count_all_results('Page');
        $config['per_page'] = 10;
        $config['use_page_numbers'] = true;
        $config['page_query_string'] = true;
        $config['query_string_segment'] = 'page';
        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] ="</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tag_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tag_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tag_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tag_close'] = "</li>";
        $config['num_links'] = 4;
        $this->pagination->initialize($config); 
        $data['pagination'] = $this->pagination->create_links();
        
        $this->db->select('p.ID, pl.Title, u.Link, pl.Text, p.Status, p.IsSystem');
        $this->db->from('Page as p');
        $this->db->join('PageLang as pl', 'pl.PageID = p.ID and pl.LangID = ' . $this->langID, 'LEFT');
        $this->db->join('Url as u', "u.ObjectID = p.ID and u.Type = 'Page'", 'LEFT');
        $this->db->order_by('ID', 'ASC');
        $this->db->offset($offset);
        $this->db->limit(10);
        $data['pages'] = $this->db->get()->result();
        
        $this->render('admin/pages', $data);
    }
    
    public function edit_page()
    {
        $id = (int)$this->input->get('id');
        
        if (count($_POST) > 0)
        {
            $link = $this->input->post('Link');
            $status = $this->input->post('Status') == 'Disabled' ? 'Disabled' : 'Active';
            $titles = $this->input->post('Title');
            $texts = $this->input->post('Text');
            $keywords = $this->input->post('Keywords');
            $descriptions = $this->input->post('Description');
            $primary_image = $this->input->post('PrimaryImage');
            
            if (empty($link))
            {
                $link = $id == 1 ? '/' : str_to_url($titles[1]);
            }
            
            // save page
            $article = [
                'Status' => $status
            ];
            
            if ($id > 0)
            {
                $this->db->update('Page', $article, ['ID' => $id]);
            }
            else
            {
                $this->db->insert('Page', $article);
                $id = $this->db->insert_id();
            }
            
            // save product langs
            $this->db->delete('PageLang', ['PageID' => $id]);
            
            $insert = [];
            foreach ($titles as $langID => $name)
            {
                $insert[] = [
                    'PageID' => $id,
                    'LangID' => $langID,
                    'Title' => $titles[$langID],
                    'Text' => $texts[$langID],
                    'Keywords' => $keywords[$langID],
                    'Description' => $descriptions[$langID]
                ];
            }
            $this->db->insert_batch('PageLang', $insert);
            
            // save link
            $this->db->select('*');
            $this->db->from('Url');
            $res = $this->db->get()->result_array();

            $all_urls = [];
            $all_urls_ot = [];
            foreach ($res as $row)
            {
                $all_urls[$row['Link']] = $row;
                $all_urls_ot[$row['Type']][$row['ObjectID']] = $row;
            }

            if (isset($all_urls[$link]))
            {
                if ($all_urls[$link]['ObjectID'] != $id || $all_urls[$link]['Type'] != 'Page')
                {
                    $i = 1;
                    $str = $link;
                    $links = array_keys($all_urls);
                    while (in_array($str, $links))
                    {
                        $str .= '-' . $i;
                        $i++;
                    }
                    $link .= '-' . $i;
                }
            }

            if (isset($all_urls_ot['Page'][$id]))
            {
                $this->db->update('Url', ['Link' => $link], ['ID' => $all_urls_ot['Page'][$id]['ID']]);
            }
            else
            {
                $this->db->insert('Url', [
                    'Link' => $link,
                    'ObjectID' => $id,
                    'Type' => 'Page'
                ]);
            }
            
            // mark primary image
            if (!empty($primary_image))
            {
                $this->db->update('PageImage', ['IsMain' => 0], ['PageID' => $id]);
                $this->db->update('PageImage', ['IsMain' => 1], ['ID' => $primary_image], 1);
            }
            
            // save images
            $config = [
                'upload_path'   => 'public/uploads/pages',
                'allowed_types' => 'jpg|png',
                'encrypt_name'  => true
            ];
            $this->load->library('upload', $config);

            if (!empty($_FILES['Images']['name']))
            {
                $images = [];
                foreach ($_FILES['Images']['name'] as $key => $image)
                {
                    $_FILES['images']['name']= $_FILES['Images']['name'][$key];
                    $_FILES['images']['type']= $_FILES['Images']['type'][$key];
                    $_FILES['images']['tmp_name']= $_FILES['Images']['tmp_name'][$key];
                    $_FILES['images']['error']= $_FILES['Images']['error'][$key];
                    $_FILES['images']['size']= $_FILES['Images']['size'][$key];

                    $this->upload->initialize($config);

                    if ($this->upload->do_upload('images'))
                    {
                        $images[] = $this->upload->data();
                    }
                }

                $config_manip = [
                    'image_library' => 'gd2',
                    'maintain_ratio' => TRUE,
                ];
                $this->load->library('image_lib', $config);

                $insert = [];
                foreach ($images as $img)
                {
                    $config_manip['source_image'] = 'public/uploads/pages/' . $img['file_name'];
                    $config_manip['new_image'] = 'public/uploads/pages/thumb_' . $img['file_name'];
                    $config_manip['width'] = 300;
                    $config_manip['height'] = 300;

                    $this->image_lib->initialize($config_manip);
                    $this->image_lib->resize();
                    $this->image_lib->clear();

                    $config_manip['source_image'] = 'public/uploads/pages/' . $img['file_name'];
                    $config_manip['new_image'] = 'public/uploads/pages/' . $img['file_name'];
                    $config_manip['width'] = 1200;
                    $config_manip['height'] = 1200;

                    $this->image_lib->initialize($config_manip);
                    $this->image_lib->resize();
                    $this->image_lib->clear();

                    $insert[] = [
                        'PageID' => $id,
                        'Image' => $img['file_name'],
                        'Thumb' => 'thumb_' . $img['file_name'],
                        'IsMain' => empty($primary_image) ? 1 : 0
                    ];

                    $primary_image = true;
                }
                
                if (count($insert) > 0)
                {
                    $this->db->insert_batch('PageImage', $insert);
                }
            }
            
            if (!empty($_FILES['Files']['name']))
            {
                 $config = [
                    'upload_path'   => 'public/uploads/pages',
                    'allowed_types' => '*',
                    'encrypt_name'  => true
                ];
                $this->load->library('upload', $config);
                
                $insert = [];
                foreach ($_FILES['Files']['name'] as $key => $image)
                {
                    $_FILES['files']['name']= $_FILES['Files']['name'][$key];
                    $_FILES['files']['type']= $_FILES['Files']['type'][$key];
                    $_FILES['files']['tmp_name']= $_FILES['Files']['tmp_name'][$key];
                    $_FILES['files']['error']= $_FILES['Files']['error'][$key];
                    $_FILES['files']['size']= $_FILES['Files']['size'][$key];

                    $this->upload->initialize($config);

                    if ($this->upload->do_upload('files'))
                    {
                        $file = $this->upload->data();
                        $insert[] = [
                            'EntityID' => $id,
                            'EntityType' => 'Page',
                            'Path' => $file['file_name'],
                            'Name' => $file['client_name']
                        ];
                    }
                }
                
                if (count($insert) > 0)
                {
                    $this->db->insert_batch('File', $insert);
                }
            }
            
            $this->session->set_flashdata('success', '<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' . lang('PageSaved') . '</div>');
                
            $red = site_url('admin/edit_page', ['id' => $id]);
            redirect($red);
        }
        
        $data['images'] = [];
        $data['files'] = [];
        if ($id > 0)
        {
            // get article
            $this->db->select('*');
            $this->db->from('Page');
            $this->db->where('ID', $id);
            $this->db->where('Status !=', 'Deleted');
            $this->db->limit(1);
            $data['article'] = $this->db->get()->row();
            
            // get article langs
            $this->db->select('*');
            $this->db->from('PageLang');
            $this->db->where('PageID', $id);
            $res = $this->db->get()->result();
            
            $data['article_langs'] = [];
            foreach ($res as $row)
            {
                $data['article_langs'][$row->LangID] = $row;
            }
            
            // get article url
            $url = $this->db->get_where('Url', ['Type' => 'Page', 'ObjectID' => $id], 1)->row();
            $data['article_link'] = isset($url->Link) ? $url->Link : '';
            
            // get article images
            $this->db->select('*');
            $this->db->from('PageImage');
            $this->db->where('PageID', $id);
            $this->db->order_by('IsMain', 'DESC');
            $data['images'] = $this->db->get()->result_array();
            
            // get article files
            $this->db->select('*');
            $this->db->from('File');
            $this->db->where('EntityID', $id);
            $this->db->where('EntityType', 'Page');
            $data['files'] = $this->db->get()->result();
        }
        
        $this->render('admin/edit_page', $data);
    }
    
    public function delete_page()
    {
        $id = (int)$this->input->get('id');
        $page = (int)$this->input->get('page');
        
        if ($id > 0)
        {
            $this->db->delete('Page', ['ID' => $id]);
            $this->db->delete('PageLang', ['PageID' => $id]);
            $this->db->delete('File', ['EntityID' => $id, 'EntityType' => 'Page']);
            $this->db->delete('PageImage', ['PageID' => $id]);
            $this->db->delete('Url', ['ObjectID' => $id, 'Type' => 'Page'], 1);
        }
        
        redirect(site_url('admin/pages'), $page > 0 ? ['page' => $page] : []);
    }
    
    public function delete_file()
    {
        $fileID = (int)$this->input->post('fileID');
        
        if ($fileID > 0)
        {
            $this->db->delete('File', ['ID' => $fileID]);
        }
    }
    
    public function orders()
    {
        $this->addBreadscrumb('', lang('Orders'));
        
        $this->load->helper('form');
        
        $ClientID = $this->input->get('ClientID');
        $Date = $this->input->get('Date');
        $PaymentType = $this->input->get('PaymentType');
        $Type = $this->input->get('Type');
        $Status = $this->input->get('Status');
        $Page = $this->input->get('page') ? (int)$this->input->get('page') : 1;
        
        $per_page = 10;
        
        $this->db->select('SQL_CALC_FOUND_ROWS *, o.ID AS OrderID', false);
        $this->db->from('Order as o');
        $this->db->join('OrderDetail as od', 'od.OrderID = o.ID', 'LEFT');
        $this->db->order_by('o.Date', 'DESC');
        if (!empty($ClientID)) $this->db->where('o.UserID', $ClientID);
        if (!empty($Date)) $this->db->where("DATE_FORMAT(o.Date, '%Y-%m-%d') =", date('Y-m-d', strtotime($Date)));
        if (!empty($PaymentType))$this->db->where('o.PaymentType', $PaymentType);
        if (!empty($Type))$this->db->where('o.Type', $Type);
        if (!empty($Status))$this->db->where('o.Status', $Status);
        $this->db->limit($per_page);
        $this->db->offset(($Page - 1) * $per_page);
        $data['orders'] = $this->db->get()->result();
        
        $total_orders = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;
        
        $this->load->library('pagination');
        $config['base_url'] = site_url('admin/orders', [], true);
        $config['total_rows'] = $total_orders;
        $config['per_page'] = $per_page;
        $config['use_page_numbers'] = true;
        $config['page_query_string'] = true;
        $config['query_string_segment'] = 'page';
        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] ="</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tag_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tag_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tag_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tag_close'] = "</li>";
        $config['num_links'] = 4;
        $get = $this->input->get();
        unset($get['page']);
        if (count($this->input->get()) > 0) $config['suffix'] = '&' . http_build_query($get);
        $config['first_url'] = $config['base_url']. '?' . http_build_query($get);
        $this->pagination->initialize($config); 
        $data['pagination'] = $this->pagination->create_links();
        
        $clients = $this->db->where_in('Type', ['Angro', 'Retail'])->get('User')->result();
        $data['clients'][''] = 'All';
        foreach ($clients as $client)
        {
            $data['clients'][$client->ID] = $client->Name;
        }
        
        $this->render('admin/orders', $data);
    }

    public function order($order_id = false)
    {
        if (!$order_id) redirect('admin/orders');
        
        $this->db->select('*');
        $this->db->from('Order as o');
        $this->db->join('OrderDetail as od', 'od.OrderID = o.ID', 'LEFT');
        $this->db->where('o.ID', $order_id);
        $data['order'] = $this->db->get()->row();
        
        if (empty($data['order'])) redirect('admin/orders');
        
        $this->db->update('Order', ['IsNew' => 0], ['ID' => $data['order']->OrderID], 1);
        
        $this->config->load('regions');
        
        $this->addBreadscrumb(site_url('admin/orders'), lang('Orders'));
        $this->addBreadscrumb('', '# ' . $data['order']->OrderID);
        
        $data['client'] = $this->db->get_where('User', ['ID' => $data['order']->UserID], 1)->row();
        
        $this->db->select('*, op.Price');
        $this->db->from('OrderProduct as op');
        $this->db->join('Product as p', 'p.ID = op.ProductID', 'LEFT');
        $this->db->join('ProductLang as pl', 'pl.ProductID = p.ID AND pl.LangID = ' . $this->langID, 'LEFT');
        $this->db->where('op.OrderID', $order_id);
        $data['order_products'] = $this->db->get()->result();
        
        $this->render('admin/order', $data);
    }
    
    public function users()
    {
        $this->addBreadscrumb('', lang('Users'));
        
        $this->load->helper('form');
        $this->config->load('regions');
        
        $UserName = $this->input->get('Email');
        $Name = $this->input->get('Name');
        $CompanyName = $this->input->get('CompanyName');
        $Region = $this->input->get('Region');
        $Type = $this->input->get('Type');
        $Status = $this->input->get('Status');
        $Page = $this->input->get('page') ? (int)$this->input->get('page') : 1;
        $per_page = 10;
        
        $this->db->select('SQL_CALC_FOUND_ROWS *', false);
        if (!empty($UserName)) $this->db->like('UserName', $UserName);
        if (!empty($Name)) $this->db->like('Name', $Name);
        if (!empty($CompanyName)) $this->db->like('CompanyName', $CompanyName);
        if (!empty($Region)) $this->db->where('Region', $Region);
        if (!empty($Type)) $this->db->where('Type', $Type);
        if (!empty($Status)) $this->db->where('Status', $Status);
        $this->db->limit($per_page);
        $this->db->offset(($Page - 1) * $per_page);
        $data['users'] = $this->db->get('User')->result();
        
        $total_users = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;
        
        $this->load->library('pagination');
        $config['base_url'] = site_url('admin/users', [], true);
        $config['total_rows'] = $total_users;
        $config['per_page'] = $per_page;
        $config['use_page_numbers'] = true;
        $config['page_query_string'] = true;
        $config['query_string_segment'] = 'page';
        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] ="</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tag_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tag_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tag_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tag_close'] = "</li>";
        $config['num_links'] = 4;
        $get = $this->input->get();
        unset($get['page']);
        if (count($this->input->get()) > 0) $config['suffix'] = '&' . http_build_query($get);
        $config['first_url'] = $config['base_url']. '?' . http_build_query($get);
        $this->pagination->initialize($config); 
        $data['pagination'] = $this->pagination->create_links();
        
        $this->render('admin/users', $data);
    }
    
    public function user($user_id = false)
    {
        $this->config->load('regions');
        $this->load->helper(['form', 'html']);
        $this->load->library('form_validation');
        
        $data['user'] = $this->db->get_where('User', ['ID' => $user_id], 1)->row();
        
        if (empty($data['user']->UserName) || $data['user']->UserName != $this->input->post('UserName'))
        {
            $this->form_validation->set_rules('UserName', lang('UserEmail'), 'trim|required|valid_email|is_unique[User.UserName]');
            $this->form_validation->set_message('is_unique', lang('EmailAllreadyExist'));
        }
        $this->form_validation->set_rules('Name', lang('Name'), 'trim|required');
        
        $password = $this->input->post('Password');
        if (!empty($password))
        {
            $this->form_validation->set_rules('Password', lang('Password'), 'matches[PasswordConfirmation]');
        }
        
        $this->form_validation->set_rules('ClientType', lang('ClientType'), 'required');
        $this->form_validation->set_rules('Region', lang('Region'), 'required|is_natural');
        
        if ($this->form_validation->run())
        {
            $clientType = $this->input->post('ClientType');
            $address = $this->input->post('Address');
            
            if (is_null($address))
            {
                $address = $this->config->item($this->input->post('Region'), 'regions');
            }
            
            $userData = [
                'UserName' => $this->input->post('UserName'),
                'Name' => $this->input->post('Name'),
                'Type' => $clientType,
                'Status' => $clientType == 'Angro' ? 'NotConfirmed' : 'Active',
                'Region' => $this->input->post('Region'),
                'Address' => $address,
                'CompanyName' => $this->input->post('CompanyName'),
                'Phone' => $this->input->post('Phone'),
                'Zip' => $this->input->post('Zip'),
                'Activitation' => $this->input->post('Activitation'),
                'RegDate' => date('c')
            ];
            
            if (!empty($password))
            {
                $userData['Password'] = $password;
            }
            
            if ($user_id > 0)
            {
                $this->db->update('User', $userData, ['ID' => $user_id], 1);
            }
            else
            {
                $this->db->insert('User', $userData);
                $user_id = $this->db->insert_id();
            }
            
            $this->session->set_flashdata('success', '<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' . lang('DataSaved') . '</div>');
            redirect('admin/user/' . $user_id);
        }
        
        $this->render('admin/user', $data);
    }
    
    public function stocks()
    {
        $this->load->helper('form');
        
        $Date = $this->input->get('Date');
        $Type = $this->input->get('Type');
        
        $this->db->select('*');
        $this->db->from('Operation as o');
        $this->db->join('ProductStock as ps', 'ps.OperationID = o.ID');
        $this->db->join('ProductLang as pl', 'pl.ProductID = ps.ProductID AND pl.LangID = ' . $this->langID);
        if (!empty($Date)) $this->db->where("DATE_FORMAT(o.Date, '%Y-%m-%d') =", date('Y-m-d', strtotime($Date)));
        if (!empty($Type)) $this->db->where('o.Type', $Type);
        $this->db->order_by('o.Date', 'DESC');
        $res = $this->db->get()->result();
        
        $stocks = [];
        foreach ($res as $row)
        {
            $stocks[$row->OperationID]['data'] = $row;
            $stocks[$row->OperationID]['ps'][] = $row->Name . ' (' . $row->Quantity . ')';
        }
        
        $this->render('admin/stocks', [
            'stocks' => $stocks
        ]);
    }
    
    public function stock($operation_id = false)
    {
        $this->load->helper('form');
        
        if (count($_POST) > 0)
        {
            $psIDs = $this->input->post('psID');
            $sold = $this->input->post('Sold');
            $quantity = $this->input->post('Quantity');
            $price = $this->input->post('Price');
            $type = $this->input->post('Type');
            $date = $this->input->post('Date');
            $date = date('c', strtotime($date));
            
            $oper = [
                'Date' => $date,
                'Type' => $type
            ];
            
            if ($operation_id > 0)
            {
                $this->db->update('Operation', $oper, ['ID' => $operation_id]);
            }
            else
            {
                $this->db->insert('Operation', $oper);
                $operation_id = $this->db->insert_id();
            }
            
            $prodIDs = $this->input->post('ProductID');
            
            $insert = [];
            foreach ($prodIDs as $key => $pID)
            {
                $insert[] = [
                    'OperationID' => $operation_id,
                    'ProductID' => $pID,
                    'Quantity' => $quantity[$key],
                    'Price' => $price[$key],
                    'Sold' => (string)$sold[$key] === '' ? $quantity[$key] : $sold[$key]
                ];
            }
            
            $this->db->delete('ProductStock', ['OperationID' => $operation_id]);
            if (count($insert) > 0)
            {
                $this->db->insert_batch('ProductStock', $insert);
            }
            
            redirect('admin/stock/' . $operation_id);
        }
        
        $operation = $this->db->get_where('Operation', ['ID' => $operation_id], 1)->row();
        
        $this->db->select('*');
        $this->db->from('Operation as o');
        $this->db->join('ProductStock as ps', 'ps.OperationID = o.ID');
        $this->db->join('Product as p', 'ps.ProductID = p.ID');
        $this->db->join('ProductLang as pl', 'pl.ProductID = p.ID AND pl.LangID = ' . $this->langID);
        $this->db->where('o.ID', $operation_id);
        $this->db->order_by('o.Date', 'DESC');
        $pstocks = $this->db->get()->result();
        
        $this->db->select('*');
        $this->db->from('Category as c');
        $this->db->join('CategoryLang as cl', 'cl.CategoryID = c.ID AND cl.LangID = ' . $this->langID);
        $this->db->order_by('cl.Name');
        $res = $this->db->get()->result();
        
        $categories = ['' => 'Selectati categorie'];
        foreach ($res as $row)
        {
            $categories[$row->CategoryID] = $row->Name;
        }
        
        $this->db->select('*');
        $this->db->from('Product as p');
        $this->db->join('ProductLang as pl', 'pl.ProductID = p.ID AND pl.LangID = ' . $this->langID);
        $this->db->order_by('pl.Name');
        $res = $this->db->get()->result();
        
        $products = ['' => 'Selectati produs'];
        foreach ($res as $row)
        {
            $products[$row->ProductID] = $row->Name;
        }
        
        $this->render('admin/stock', [
            'operation' => $operation,
            'pstocks' => $pstocks,
            'categories' => $categories,
            'products' => $products
        ]);
    }
    
    public function get_product_by_category()
    {
        $id = $this->input->post('id');
        
        $this->load->helper('form');
        
        $this->db->select('*');
        $this->db->from('Product as p');
        $this->db->join('ProductLang as pl', 'pl.ProductID = p.ID AND pl.LangID = ' . $this->langID);
        $this->db->where('p.CategoryID', $id);
        $this->db->order_by('pl.Name');
        $res = $this->db->get()->result();
        
        $products = ['' => 'Selectati produs'];
        foreach ($res as $row)
        {
            $products[$row->ProductID] = $row->Name;
        }
        
        echo form_dropdown('ProductID[]', $products, 0, 'class="form-control" required');
    }
    
    public function edit_order($order_id = 0)
    {
        $this->config->load('regions');
        $this->load->helper('form');
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('name', lang('Name'), 'required');
        $this->form_validation->set_rules('region', lang('Region'), 'required|integer');
        $this->form_validation->set_rules('address', lang('Address'), 'required');
        $this->form_validation->set_rules('zip', lang('Zip'), 'required');
        $this->form_validation->set_rules('phone', lang('Phone'), 'required');
        $this->form_validation->set_rules('email', lang('Email'), 'trim|required|valid_email');
        $this->form_validation->set_rules('payment', lang('PaymentType'), 'required');
        
        if ($this->form_validation->run())
        {
            $paymentType = $this->input->post('payment') == 'card' ? 'Card' : 'Cash';
            $order = [
                'UserID' => $this->input->post('ClientID') ? $this->input->post('ClientID') : null,
                'Amount' => 0,
                'Date' => date('c'),
                'Type' => $this->shopType,
                'PaymentType' => $paymentType,
                'Status' => 'Pending'
            ];
            $this->db->insert('Order', $order);
            $order_id = $this->db->insert_id();

            $orderDetails = [
                'OrderID' => $order_id,
                'Name' => $this->input->post('name'),
                'Region' => $this->input->post('region'),
                'Address' => $this->input->post('address'),
                'Zip' => $this->input->post('zip'),
                'Phone' => $this->input->post('phone'),
                'Email' => $this->input->post('email'),
            ];
            $this->db->insert('OrderDetail', $orderDetails);

            $total = 0;
            $order_products = [];
            foreach ($_POST['CategoryID'] as $key => $val)
            {
                $total += $_POST['Price'][$key] * $_POST['Quantity'][$key];
                $order_products[] = [
                    'OrderID' => $order_id,
                    'ProductID' => $_POST['ProductID'][$key],
                    'Price' => $_POST['Price'][$key],
                    'Quantity' => $_POST['Quantity'][$key]
                ];
            }
            $this->db->insert_batch('OrderProduct', $order_products);
            
            $this->db->update('Order', ['Amount' => $total], ['ID' => $order_id]);
            
            $oper = [
                'Date' => date('c'),
                'Type' => 'Sale',
                'OrderID' => $order_id
            ];
            
            $this->db->insert('Operation', $oper);
            $operation_id = $this->db->insert_id();
            
            $order_stock = [];
            foreach ($_POST['CategoryID'] as $key => $val)
            {
                $order_stock[] = [
                    'OperationID' => $operation_id,
                    'ProductID' => $_POST['ProductID'][$key],
                    'Price' => $_POST['Price'][$key],
                    'Quantity' => $_POST['Quantity'][$key],
                    'Sold' => 0
                ];
            }
            $this->db->insert_batch('ProductStock', $order_stock);
            
            redirect('admin/orders');
        }
        
        $order = $this->db->get_where('Order', ['ID' => $order_id], 1)->row();
        $order_details = $this->db->get_where('OrderDetail', ['OrderID' => $order_id], 1)->row();
        $user = $this->db->get_where('User', ['ID' => isset($order->UserID) ? $order->UserID : '-1'], 1)->row();
        
        $res = $this->db->where_in('Type', ['Angro', 'Retail'])->get('User')->result();
        $clients[''] = 'Selectati client';
        foreach ($res as $client)
        {
            $clients[$client->ID] = $client->Name;
        }
        
        $this->db->select('*, op.Price');
        $this->db->from('OrderProduct as op');
        $this->db->join('Product as p', 'p.ID = op.ProductID', 'LEFT');
        $this->db->join('ProductLang as pl', 'pl.ProductID = p.ID AND pl.LangID = ' . $this->langID, 'LEFT');
        $this->db->where('op.OrderID', $order_id);
        $order_products = $this->db->get()->result();
        
        $this->db->select('*');
        $this->db->from('Category as c');
        $this->db->join('CategoryLang as cl', 'cl.CategoryID = c.ID AND cl.LangID = ' . $this->langID);
        $this->db->order_by('cl.Name');
        $res = $this->db->get()->result();
        
        $categories = ['' => 'Selectati categorie'];
        foreach ($res as $row)
        {
            $categories[$row->CategoryID] = $row->Name;
        }
        
        $this->db->select('*');
        $this->db->from('Product as p');
        $this->db->join('ProductLang as pl', 'pl.ProductID = p.ID AND pl.LangID = ' . $this->langID);
        $this->db->order_by('pl.Name');
        $res = $this->db->get()->result();
        
        $products = ['' => 'Selectati produs'];
        foreach ($res as $row)
        {
            $products[$row->ProductID] = $row->Name;
        }
        
        $this->render('admin/edit_order', [
            'order' => $order,
            'user' => $user,
            'clients' => $clients,
            'order_details' => $order_details,
            'order_products' => $order_products,
            'products' => $products,
            'categories' => $categories
        ]);
    }
    
    public function get_product_price()
    {
        $id = $this->input->post('id');
        
        $res = $this->db->get_where('Product', ['ID' => $id], 1)->row();
        
        echo empty($res->IsPromo) ? $res->Price : $res->DiscountPrice;
    }
    
    public function get_user()
    {
        $id = $this->input->post('id');
        
        $user = $this->db->get_where('User', ['ID' => $id], 1)->row();
        
        echo json_encode($user);
    }
    
}