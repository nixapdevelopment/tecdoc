<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends FrontController
{

    public function __construct()
    {
        parent::__construct();
        
        $this->load->model('CategoryModel');
        $this->data['categories'] = $this->CategoryModel->getTree($this->langID, true);
        
        if (!empty($_SESSION['cart']) > 0)
        {
            foreach ($_SESSION['cart'] as $pID => $data)
            {
                $this->cartAmount += $_SESSION['cart'][$pID]['price'] * $_SESSION['cart'][$pID]['quantity'];
            }
        }
    }

    public function index()
    {
        $this->db->select('*');
        $this->db->from('News as n');
        $this->db->join('NewsLang as nl', 'nl.NewsID = n.ID AND nl.LangID = ' . $this->langID);
        $this->db->join("Url as u", "u.ObjectID = n.ID AND u.Type = 'News'");
        $this->db->order_by('n.Date', 'DESC');
        $this->db->limit(4);
        $data['news'] = $this->db->get()->result();
        
        $this->render('home', $data);
    }
    
    public function login()
    {
        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('UserName', lang('UserEmail'), 'required|valid_email');
        $this->form_validation->set_rules('Password', lang('Password'), 'required');
        
        if ($this->form_validation->run())
        {
            $this->form_validation->set_rules('Password', '', 'callback_do_user_login');
            
            $this->form_validation->run();
        }
        
        $this->addBreadscrumb('', lang('Login'));
        
        $this->render('login');
    }
    
    public function do_user_login()
    {
        $this->load->model('UserModel');
        
        $username = $this->input->post('UserName');
        $password = $this->input->post('Password');
        
        $user = $this->UserModel->getForLogin($username, $password);
        
        if (isset($user->ID))
        {
            $this->session->set_userdata('UserID', $user->ID);
            
            switch ($user->Type)
            {
                case 'Retail':
                    redirect(site_url('user'));
                    break;
                case 'Angro':
                    if ($user->Status == 'Active')
                    {
                        redirect(site_url('user'));
                    }
                    redirect('wait-confirmation');
                    break;
                case 'Admin':
                    redirect(site_url('admin'));
                    break;
            }
        }
        
        $this->form_validation->set_message('do_user_login', lang('InvalidLogin'));
        
        return false;
    }

    public function password_recovery()
    {
        $this->load->helper('form');
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('UserName', lang('UserEmail'), 'trim|required|valid_email');
        
        if ($this->form_validation->run())
        {
            $this->form_validation->set_rules('UserName', lang('UserEmail'), 'callback_do_password_recovery');
            
            $this->form_validation->run();
        }
        
        $this->addBreadscrumb('', lang('PasswordRecovery'));
        
        $this->render('password_recovery');
    }
    
    public function do_password_recovery()
    {
        $this->load->model('UserModel');
        
        $username = $this->input->post('UserName');
        
        $user = $this->UserModel->getByUsername($username);
        
        if (isset($user->ID))
        {
            $token = md5(uniqid());
            $this->db->update('User', ['Token' => $token], ['ID' => $user->ID]);
            
            $this->load->library('email');
            $this->email->from('system@agm.md', 'AGM');
            $this->email->to($user->UserName);
            $this->email->subject(lang('RecoveryMessageSubject'));
            $this->email->message('<a href="' . site_url('reset-password', ['token' => $token]) . '">Link</a> Token: ' . $token);
            $this->email->send();
            
            redirect('reset-password');
        }
        
        $this->form_validation->set_message('do_password_recovery', lang('EmailNotRegistered'));
        
        return false;
    }
    
    public function registration()
    {
        $this->config->load('regions');
        $this->load->helper(['form', 'html']);
        $this->load->library('form_validation');

        $this->form_validation->set_rules('UserName', lang('UserEmail'), 'trim|required|valid_email|is_unique[User.UserName]');
        $this->form_validation->set_rules('Name', lang('Name'), 'trim|required');
        $this->form_validation->set_message('is_unique', lang('EmailAllreadyExist'));
        $this->form_validation->set_rules('Password', lang('Password'), 'required|min_length[5]|matches[PasswordConfirmation]');
        $this->form_validation->set_rules('PasswordConfirmation', lang('PasswordConfirmation'), 'required|min_length[5]');
        $this->form_validation->set_rules('ClientType', lang('ClientType'), 'required');
        $this->form_validation->set_rules('Region', lang('Region'), 'required|is_natural');
        
        if ($this->form_validation->run())
        {
            $clientType = $this->input->post('ClientType') == 'Angro' ? 'Angro' : 'Retail';
            $address = $this->input->post('Address');
            
            if (is_null($address))
            {
                $address = $this->config->item($this->input->post('Region'), 'regions');
            }
            
            $this->db->insert('User', [
                'UserName' => $this->input->post('UserName'),
                'Password' => $this->input->post('Password'),
                'Name' => $this->input->post('Name'),
                'Type' => $clientType,
                'Status' => $clientType == 'Angro' ? 'NotConfirmed' : 'Active',
                'Region' => $this->input->post('Region'),
                'Address' => $address,
                'CompanyName' => $this->input->post('CompanyName'),
                'Phone' => $this->input->post('Phone'),
                'Zip' => $this->input->post('Zip'),
                'Activitation' => $this->input->post('Activitation'),
                'RegDate' => date('c')
            ]);
            $user_id = $this->db->insert_id();
            
            if (!empty($_FILES['Files']))
            {
                $config = array(
                    'upload_path'   => 'public/uploads/files',
                    'allowed_types' => 'pdf|doc|docx|xls|xlsx|png|jpg|gif|tiff|tif',
                    'encrypt_name' => true
                );
                $this->load->library('upload', $config);

                $files_insert = array();
                foreach ($_FILES['Files']['name'] as $key => $name)
                {
                    $_FILES['file']['name'] = $_FILES['Files']['name'][$key];
                    $_FILES['file']['type'] = $_FILES['Files']['type'][$key];
                    $_FILES['file']['tmp_name'] = $_FILES['Files']['tmp_name'][$key];
                    $_FILES['file']['error'] = $_FILES['Files']['error'][$key];
                    $_FILES['file']['size'] = $_FILES['Files']['size'][$key];

                    if ($this->upload->do_upload('file'))
                    {
                        $file_data = $this->upload->data();
                        $files_insert[] = [
                            'EntityID' => $user_id,
                            'EntityType' => 'User',
                            'Path' => $file_data['file_name'],
                            'Name' => $file_data['client_name']
                        ];
                    }
                }
                
                if (count($files_insert) > 0)
                {
                    $this->db->insert_batch('File', $files_insert);
                }
            }
            
            $this->session->set_userdata('UserID', $user_id);
            
            if ($clientType == 'Angro')
            {
                redirect('wait-confirmation');
            }
            
            redirect('user');
        }
        
        $this->addBreadscrumb('', lang('Registration'));
        
        $this->render('registration');
    }
    
    public function wait_confirmation()
    {
        $this->addBreadscrumb('', lang('WaitConfirmation'));
        
        $this->render('wait_confirmation');
    }
    
    public function logout()
    {
        $this->session->unset_userdata('UserID');
        redirect();
    }
    
    public function reset_password()
    {
        $this->load->helper(['form', 'html']);
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('Token', lang('RecoveryToken'), 'trim|required|callback_check_recovery_token');
        $this->form_validation->set_rules('Password', lang('Password'), 'required|min_length[5]|matches[PasswordConfirmation]');
        $this->form_validation->set_rules('PasswordConfirmation', lang('PasswordConfirmation'), 'required|min_length[5]');
        
        if ($this->form_validation->run())
        {
            $token = $this->input->post('Token');
            
            $update = [];
            $update['Token'] = '';
            $update['Password'] = $this->input->post('Password');
            
            $this->db->update('User', $update, ['Token' => $token]);
            
            $this->session->set_flashdata('password_recovered', true);
            
            redirect('login');
        }
        
        $this->addBreadscrumb('', lang('ResetPassword'));
        
        $this->render('reset_password');
    }
    
    public function check_recovery_token()
    {
        $token = $this->input->post('Token');
        
        $user = $this->db->get_where('User', ['Token' => $token])->row();
        
        if (isset($user->ID))
        {
            return true;
        }
        
        $this->form_validation->set_message('check_recovery_token', lang('InvalidRecoveryToken'));
        
        return false;
    }
    
    public function subscribe()
    {
        $email = $this->input->post('Email');
        
        $subscribe = $this->db->get_where('Subscribe', ['Email' => $email], 1)->row();
        
        if (isset($subscribe->Email))
        {
            exit('<div class="text-danger">' . lang('AllreadySubscribed') . '</div>');
        }
        
        $this->db->insert('Subscribe', ['Email' => $email]);
        
        exit('<div class="text-success">' . lang('SubscribeSuccess') . '</div>');
    }
    
    public function router($url = '/')
    {
        $url = $this->db->get_where('Url', ['Link' => $url], 1)->row();
        
        if (empty($url->ID))
        {
            show_404();
        }
        
        switch ($url->Type) 
        {
            case 'Page':
                $this->page($url->ObjectID);
                break;
            case 'News':
                $this->article($url->ObjectID);
                break;
            case 'Category':
                $this->category($url->ObjectID);
                break;
            case 'Product':
                $this->product($url->ObjectID);
                break;
            default:
                show_404();
                break;
        }
    }
    
    private function page($id)
    {
        // get page
        $this->db->select('*');
        $this->db->from('Page as p');
        $this->db->join('PageLang as pl', 'pl.PageID = p.ID and pl.LangID = ' . $this->langID, 'LEFT');
        $this->db->where('ID', $id);
        $this->db->limit(1);
        $page = $this->db->get()->row();
        
        // if page not found or not active
        if (empty($page->ID) || $page->Status != 'Active')
        {
            show_404();
        }
        
        $data['page'] = $page;
        
        // get images
        $data['images'] = $this->db->get_where('PageImage', ['PageID' => $page->ID])->result();
        
        // get files
        $data['files'] = $this->db->get_where('File', ['EntityID' => $page->ID, 'EntityType' => 'Page'])->result();
        
        $this->addBreadscrumb('', $page->Title);
        
        switch ($page->Template)
        {
            case 'home';
                $this->index($data);
                break;
            case 'catalog';
                $this->catalog($data);
                break;
            case 'news';
                $this->news($data);
                break;
            case 'contacts';
                $this->contacts($data);
                break;
            case 'chain';
                $this->chain($data);
                break;
            default:
                $this->render('page', $data);
                break;
        }
    }
    
    private function article($id)
    {
        $this->db->select('*');
        $this->db->from('News as n');
        $this->db->join('NewsLang as nl', 'nl.NewsID = n.ID AND nl.LangID = ' . $this->langID);
        $this->db->where('n.ID', $id);
        $this->db->limit(1);
        $data['article'] = $this->db->get()->row();
        
        $data['images'] = $this->db->get_where('NewsImage', ['NewsID' => $id])->result();
        $data['files'] = $this->db->get_where('File', ['EntityID' => $id, 'EntityType' => 'News'])->result();
        
        $this->render('article', $data);
    }
    
    public function news()
    {
        $this->load->library('pagination');
        
        $per_page = 10;
        
        $config['base_url'] = site_url('news');
        $config['total_rows'] = $this->db->count_all('News');
        $config['per_page'] = $per_page;
        $config['use_page_numbers'] = true;
        $config['page_query_string'] = true;
        $config['query_string_segment'] = 'page';
        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] ="</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tag_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tag_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tag_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tag_close'] = "</li>";
        $config['num_links'] = 4;
        $this->pagination->initialize($config); 
        $data['pagination'] = $this->pagination->create_links();
        
        $page = $this->input->get('page') ? (int)$this->input->get('page') : 1;
        $offset = ($page - 1) * $per_page;
        
        $this->db->select('n.ID, nl.Title, nl.Text, n.Date, n.Status, u.Link');
        $this->db->from('News as n');
        $this->db->join('NewsLang as nl', 'nl.NewsID = n.ID and nl.LangID = ' . $this->langID, 'LEFT');
        $this->db->join('Url as u', "u.ObjectID = n.ID and u.Type = 'News'", 'LEFT');
        $this->db->order_by('n.Date', 'DESC');
        $this->db->offset($offset);
        $this->db->limit($per_page);
        $data['news'] = $this->db->get()->result();
        
        $this->render('news', $data);
    }

    private function category($id)
    {
        $this->load->model('CategoryModel');
        
        // get category
        $data['category'] = $this->CategoryModel->getByID($id, $this->langID);
        $data['category_lang'] = $this->db->get_where('CategoryLang', ['CategoryID' => $id, 'LangID' => $this->langID])->row();
        
        // get subcategories
        $data['subcategories'] = $this->CategoryModel->getByParent($id, $this->langID);
        
        // menu
        $data['categories_menu'] = $this->CategoryModel->getTree($this->langID, true, $data['category']->ID);
        $data['in_categories_menu'] = $this->CategoryModel->getTree($this->langID, false, false, true);
        
        // get category products
        $this->db->select('*');
        $this->db->from('Product');
        $this->db->where('CategoryID', $id);
        $this->db->where('Status', 'Active');
        $this->db->where_in('Type', ['All', $this->shopType]);
        $res = $this->db->get()->result();
        
        $productIDs = [0];
        $data['min_price'] = 0;
        $data['max_price'] = 0;
        $data['filters'] = [];
        foreach ($res as $row)
        {
            $productIDs[$row->ID] = $row->ID;
            
            $price = $row->IsPromo == 1 && strtotime($row->DiscountPriceStart) < time() && strtotime($row->DiscountPriceEnd) > time() ? $row->DiscountPrice : $row->Price;

            if ($data['min_price'] == 0 || $price < $data['min_price'])
            {
                $data['min_price'] = $price;
            }
            
            if ($data['max_price'] == 0 || $price > $data['max_price'])
            {
                $data['max_price'] = $price;
            }
        }
        
        // get filters
        $this->db->select('f.ID, f.Type, f.Unit, fl.Name, pfl.Value');
        $this->db->from('ProductFilter as pf');
        $this->db->join('ProductFilterLang as pfl', 'pfl.ProductFilterID = pf.ID', 'LEFT');
        $this->db->join('Filter as f', 'f.ID = pf.FilterID', 'LEFT');
        $this->db->join('FilterLang as fl', 'fl.FilterID = f.ID and fl.LangID = ' . $this->langID, 'LEFT');
        $this->db->where_in('pf.ProductID', $productIDs);
        $this->db->order_by('f.System, fl.Name');
        $res = $this->db->get()->result();
        
        foreach ($res as $row)
        {
            if (!isset($data['filters'][$row->ID]['min']))
            {
                $data['filters'][$row->ID]['min'] = 0;
            }
            if (!isset($data['filters'][$row->ID]['max']))
            {
                $data['filters'][$row->ID]['max'] = 0;
            }
            
            $data['filters'][$row->ID]['data'] = $row;
            $data['filters'][$row->ID]['results'][$row->Value] = $row->Value;
            $data['filters'][$row->ID]['min'] = $data['filters'][$row->ID]['min'] == 0 || $row->Value < $data['filters'][$row->ID]['min'] ? $row->Value : $data['filters'][$row->ID]['min'];
            $data['filters'][$row->ID]['max'] = $data['filters'][$row->ID]['max'] == 0 || $row->Value > $data['filters'][$row->ID]['max'] ? $row->Value : $data['filters'][$row->ID]['max'];
        }
        
        $this->render('category', $data);
    }
    
    public function ajaxProducts()
    {
        if (!$this->input->is_ajax_request()) exit;
        
        parse_str($this->input->post('query'), $where_arr);
        
        $query = $this->db->get('Filter');
        $filters = [];
        foreach ($query->result() as $filter)
        {
            $filters[$filter->ID] = $filter->Type;
        }
        
        $this->db->select("SQL_CALC_FOUND_ROWS count(p.ID) as `count`, pf.ProductID, pl.Name, pi.Image, p.Sku, pi.Thumb, IF(`p`.`IsPromo` = 1, p.DiscountPrice, p.Price) as `ProductPrice`, p.IsPromo, u.Link", false);
        $this->db->from('ProductFilterLang as pfl');
        $this->db->join('ProductFilter as pf', 'pf.ID = pfl.ProductFilterID AND pfl.LangID = ' . $this->langID, 'RIGHT');
        $this->db->join('Product as p', "p.ID = pf.ProductID and p.`Status` = 'Active' and p.Type IN ('All', '" . $this->shopType . "')", 'RIGHT');
        $this->db->join('ProductLang as pl', 'p.ID = pl.ProductID and pl.LangID = ' . $this->langID, 'RIGHT');
        $this->db->join("Url as u", "u.ObjectID = p.ID and u.Type = 'Product'", 'RIGHT');
        $this->db->join('ProductImage as pi', 'pi.ProductID = p.ID and pi.IsMain = 1', 'RIGHT');
        
        // filters query
        $i = 0;
        if (!empty($where_arr['f']))
        {
            foreach ($where_arr['f'] as $key => $val)
            {
                if ($filters[$key] == 'Number')
                {
                    $i++;
                    $val = explode(',', $val);
                    $minvalue = isset($val[0]) ? (int)$val[0] : 0;
                    $maxvalue = isset($val[1]) ? (int)$val[1] : 99999999999;
                    $this->db->or_where("(`pf`.`FilterID` = $key AND (`pfl`.`Value` >= $minvalue AND `pfl`.`Value` <=  $maxvalue))");
                }
                elseif ($filters[$key] == 'String')
                {
                    $i++;
                    $in = implode("','", $val);
                    if (!empty($in))
                    {
                        $this->db->or_where("(`pf`.`FilterID` = $key AND `pfl`.`Value` IN ('$in'))");
                    }
                }
                elseif ($filters[$key] == 'Price')
                {
                    $val = explode(',', $val);
                    $minvalue = isset($val[0]) ? (int)$val[0] : 0;
                    $maxvalue = isset($val[1]) ? (int)$val[1] : 99999999999;
                    $this->db->having("(`ProductPrice` >= $minvalue AND `ProductPrice` <= $maxvalue)");
                }
            }
        }
        
        if ($i > 0)
        {
            $this->db->having('count', $i);
        }
        
        $this->db->group_by('p.ID');
        
        // order query
        switch ($where_arr['sort'])
        {
            case 'name':
                $this->db->order_by('pl.Name', 'asc');
                break;
            case 'date':
                $this->db->order_by('p.ID', 'desc');
                break;
            default:
                $this->db->order_by('ProductPrice', 'asc');
                break;
        }
        
        // page and per_page query
        $where_arr['page'] = $where_arr['page'] > 0 ? $where_arr['page'] : 1;
        $this->db->limit($where_arr['per_page']);
        $this->db->offset(($where_arr['page'] - 1) * $where_arr['per_page']);
        
        $products = $this->db->get()->result();
        $query = $this->db->simple_query('select FOUND_ROWS()')->fetch_row();
        $count_all = reset($query);
        
        echo $this->load->view('ajax_products', [
            'products' => $products,
            'count' => $count_all,
            'page' => $where_arr['page'],
            'per_page' => $where_arr['per_page']
        ], true);
    }

    private function product($id)
    {
        $this->db->select('*, IF(`p`.`IsPromo` = 1, p.DiscountPrice, p.Price) as `ProductPrice`');
        $this->db->from('Product as p');
        $this->db->join('ProductLang as pl', 'p.ID = pl.ProductID and pl.LangID = ' . $this->langID);
        $this->db->join("Url as u", "u.ObjectID = p.ID and u.Type = 'Product'");
        $this->db->where('p.ID', $id);
        $this->db->limit(1);
        $product = $this->db->get()->row();
        
        if (empty($product->ID)) redirect();
        
        // images
        $this->db->order_by('IsMain', 'DESC');
        $images = $this->db->get_where('ProductImage', ['ProductID' => $product->ProductID])->result();
        
        // filters
        $this->db->select('*');
        $this->db->from('ProductFilter as pf');
        $this->db->join('ProductFilterLang as pfl', 'pfl.ProductFilterID = pf.ID AND pfl.LangID = ' . $this->langID);
        $this->db->join('FilterLang as fl', 'fl.FilterID = pf.FilterID AND fl.LangID = ' . $this->langID);
        $this->db->where('pf.ProductID', $product->ProductID);
        $this->db->group_by('pf.FilterID');
        $filters = $this->db->get()->result();
        
        $this->render('product', [
            'product' => $product,
            'images' => $images,
            'filters' => $filters
        ]);
    }
    
    public function cart_add($update = 0)
    {
        $productID = $this->input->post('productID');
        $quantity = $this->input->post('quantity') ? $this->input->post('quantity') : 1;
        
        $this->db->select('*');
        $this->db->from('Product as p');
        $this->db->where('p.ID', $productID);
        $this->db->limit(1);
        $product = $this->db->get()->row();
        
        $amount = 0;
        $errors = [];
        if (($product->Stock - $quantity) < 0)
        {
            $errors[] = lang('OutOfStock');
        }
        
        if (count($errors) == 0)
        {
            if (isset($_SESSION['cart'][$product->ID]['quantity']))
            {
                $_SESSION['cart'][$product->ID]['quantity'] = $update ? $quantity : $_SESSION['cart'][$product->ID]['quantity'] + $quantity;
                $_SESSION['cart'][$product->ID]['price'] = $product->IsPromo == 1 && strtotime($product->DiscountPriceStart) < time() && time() < strtotime($product->DiscountPriceEnd) ? $product->DiscountPrice : $product->Price;
                
            }
            else
            {
                $_SESSION['cart'][$product->ID] = [
                    'quantity' => $quantity,
                    'price' => $product->IsPromo == 1 && strtotime($product->DiscountPriceStart) < time() && time() < strtotime($product->DiscountPriceEnd) ? $product->DiscountPrice : $product->Price
                ];
            }
        }
        
        if (count($_SESSION['cart']) > 0)
        {
            foreach ($_SESSION['cart'] as $pID => $data)
            {
                $amount += $_SESSION['cart'][$pID]['price'] * $_SESSION['cart'][$pID]['quantity'];
            }
        }
        
        exit(json_encode([
            'result' => count($errors) > 0 ? 'error' : 'success',
            'messages' => count($errors) > 0 ? implode('<br>', $errors) : ($update ? lang('CartChanged') : lang('AddedToCart')),
            'amount' => number_format($amount, 2)
        ]));
    }
    
    public function cart()
    {
        if (count($_POST) > 0)
        {
            redirect('checkout');
        }
        
        $cart = $this->session->userdata('cart');
        
        $contents = [];
        $total = 0;
        if (!empty($cart))
        {
            $cartProdIDs = array_keys($cart);
            
            $this->db->select('*, pl.Name as Name, cl.Name as CategoryName');
            $this->db->from('Product as p');
            $this->db->join('ProductLang as pl', 'p.ID = pl.ProductID and pl.LangID = ' . $this->langID, 'LEFT');
            $this->db->join('CategoryLang as cl', 'p.CategoryID = cl.CategoryID and cl.LangID = ' . $this->langID, 'LEFT');
            $this->db->join("Url as u", "u.ObjectID = p.ID and u.Type = 'Product'", 'LEFT');
            $this->db->join('ProductImage as pi', 'pi.ProductID = p.ID and pi.IsMain = 1', 'LEFT');
            $this->db->where_in('p.ID', $cartProdIDs);
            $products = $this->db->get()->result();
            
            foreach ($products as $item)
            {
                $total += $cart[$item->ProductID]['quantity'] * $cart[$item->ProductID]['price'];
                $contents[$item->ProductID] = [
                    'ID' => $item->ProductID,
                    'Name' => $item->Name,
                    'CategoryName' => $item->CategoryName,
                    'Thumb' => $item->Thumb,
                    'Image' => $item->Image,
                    'Link' => $item->Link,
                    'Quantity' => $cart[$item->ProductID]['quantity'],
                    'Price' => $cart[$item->ProductID]['price'],
                    'Subtotal' => number_format($cart[$item->ProductID]['quantity'] * $cart[$item->ProductID]['price'], 2),
                    'Stock' => $item->Stock
                ];
            }
        }
        
        $this->render('cart', [
            'contents' => $contents,
            'total' => $total
        ]);
    }
    
    public function cart_delete()
    {
        $productID = $this->input->post('productID');
        
        $amount = 0;
        
        unset($_SESSION['cart'][$productID]);
        
        if (count($_SESSION['cart']) > 0)
        {
            foreach ($_SESSION['cart'] as $pID => $data)
            {
                $amount += $_SESSION['cart'][$pID]['price'] * $_SESSION['cart'][$pID]['quantity'];
            }
        }
        
        exit(json_encode([
            'result' => 'success',
            'messages' => lang('CartProductDeleted'),
            'amount' => number_format($amount, 2)
        ]));
    }
    
    public function checkout()
    {
        if (!isset($_SESSION['cart']) || count($_SESSION['cart']) == 0)
        {
            redirect('cart');
        }
     
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->config->load('regions');
        
        $user = $this->db->get_where('User', ['ID' => $this->session->userdata('UserID')], 1)->row();
        
        $cart = $this->session->userdata('cart');
        
        $contents = [];
        $total = 0;
        
        $cartProdIDs = array_keys($cart);

        $this->db->select('*, pl.Name as Name, cl.Name as CategoryName');
        $this->db->from('Product as p');
        $this->db->join('ProductLang as pl', 'p.ID = pl.ProductID and pl.LangID = ' . $this->langID, 'LEFT');
        $this->db->join('CategoryLang as cl', 'p.CategoryID = cl.CategoryID and cl.LangID = ' . $this->langID, 'LEFT');
        $this->db->join("Url as u", "u.ObjectID = p.ID and u.Type = 'Product'", 'LEFT');
        $this->db->join('ProductImage as pi', 'pi.ProductID = p.ID and pi.IsMain = 1', 'LEFT');
        $this->db->where_in('p.ID', $cartProdIDs);
        $products = $this->db->get()->result();

        foreach ($products as $item)
        {
            $total += $cart[$item->ProductID]['quantity'] * $cart[$item->ProductID]['price'];
            $contents[$item->ProductID] = [
                'ID' => $item->ProductID,
                'Name' => $item->Name,
                'CategoryName' => $item->CategoryName,
                'Thumb' => $item->Thumb,
                'Image' => $item->Image,
                'Link' => $item->Link,
                'Quantity' => $cart[$item->ProductID]['quantity'],
                'Price' => $cart[$item->ProductID]['price'],
                'Subtotal' => number_format($cart[$item->ProductID]['quantity'] * $cart[$item->ProductID]['price'], 2),
                'Stock' => $item->Stock
            ];
        }
        
        $this->form_validation->set_rules('name', lang('Name'), 'required');
        $this->form_validation->set_rules('region', lang('Region'), 'required|integer');
        $this->form_validation->set_rules('address', lang('Address'), 'required');
        $this->form_validation->set_rules('zip', lang('Zip'), 'required');
        $this->form_validation->set_rules('phone', lang('Phone'), 'required');
        $this->form_validation->set_rules('email', lang('Email'), 'trim|required|valid_email');
        $this->form_validation->set_rules('payment', lang('PaymentType'), 'required');
        
        if ($this->form_validation->run())
        {
            $paymentType = $this->input->post('payment') == 'card' ? 'Card' : 'Cash';
            $order = [
                'UserID' => $this->session->userdata('UserID') ? $this->session->userdata('UserID') : null,
                'Amount' => $total,
                'Date' => date('c'),
                'Type' => $this->shopType,
                'PaymentType' => $paymentType,
                'Status' => 'Pending'
            ];
            $this->db->insert('Order', $order);
            $order_id = $this->db->insert_id();
            
            $orderDetails = [
                'OrderID' => $order_id,
                'Name' => $this->input->post('name'),
                'Region' => $this->input->post('region'),
                'Address' => $this->input->post('address'),
                'Zip' => $this->input->post('zip'),
                'Phone' => $this->input->post('phone'),
                'Email' => $this->input->post('email'),
            ];
            $this->db->insert('OrderDetail', $orderDetails);
            
            $order_products = [];
            foreach ($cart as $productID => $cData)
            {
                $order_products[] = [
                    'OrderID' => $order_id,
                    'ProductID' => $productID,
                    'Price' => $cData['price'],
                    'Quantity' => $cData['quantity']
                ];
            }
            $this->db->insert_batch('OrderProduct', $order_products);
            
            $this->session->unset_userdata('cart');
            
            if ($paymentType == 'Cash')
            {
                redirect('thankyou/' . $order_id);
            }
            else
            {
                // redirect to cart processing
            }
        }
        
        $this->render('checkout', [
            'user' => $user,
            'contents' => $contents,
            'total' => $total
        ]);
    }
    
    public function thankyou($order_id = false)
    {
        if (!$order_id)
        {
            redirect('cart');
        }
        
        $order = $this->db->get_where('Order', ['ID' => $order_id], 1)->row();
        
        if (empty($order->ID) || ($order->Status == 'Paid' && $order->PaymentType == 'Cash'))
        {
            redirect('cart');
        }
        
        $this->render('thankyou', [
            'order' => $order
        ]);
    }
    
    public function product_search()
    {
        $query = $this->input->get('q');
        
        $this->db->select('*');
        $this->db->from('Product as p');
        $this->db->join('ProductLang as pl', 'pl.ProductID = p.ID AND pl.LangID = ' . $this->langID);
        $this->db->join('ProductImage as pi', 'pi.ProductID = p.ID AND pi.IsMain = 1');
        $this->db->join('Url as u', "u.ObjectID = p.ID AND u.Type = 'Product'");
        $this->db->like('Sku', $query);
        $products = $this->db->get()->result();
        
        $return = [];
        foreach ($products as $product)
        {
            $return[] = [
                'id' => $product->Link,
                'text' => '<div class="row"><div class="col-sm-3"><img src="' . base_url('public/uploads/products/' . $product->Thumb) . '" class="img-thumbnail" style="height: 50px;" /></div><div class="col-sm-9">' .  $product->Name . '<br>SKU: ' . $product->Sku . '</div></div>'
            ];
        }
        
        exit(json_encode($return));
    }
    
}