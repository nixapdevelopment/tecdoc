<table class="prod-table table table-hover">
    <thead>
        <tr>
            <th style="width: 100px;">Фото</th>
            <th>Артикул, №</th>
            <th>Наименование</th>
            <th>Цена</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($products as $product) { ?>
            <tr>
                <td class="prod-thumb"><a class="fancybox" href="<?= base_url('public/uploads/products/' . $product->Image) ?>"><img src="<?= base_url('public/uploads/products/' . $product->Thumb) ?>" alt="<?= $product->Name ?>" /></a></td>
                <td><a href="<?= site_url($product->Link) ?>"><?= $product->Sku ?></a></td>
                <td><a href="<?= site_url($product->Link) ?>"><?= $product->Name ?></a></td>
                <td><?= number_format($product->ProductPrice, 2) ?> lei</td>
                <td><a href="<?= site_url($product->Link) ?>" class="btn btn-md btn-danger">Посмотреть</a></td>
            </tr>
        <?php } ?>
        <?php foreach ($products as $product) { ?>
            <tr>
                <td class="prod-thumb"><a class="fancybox" href="<?= base_url('public/uploads/products/' . $product->Image) ?>"><img src="<?= base_url('public/uploads/products/' . $product->Thumb) ?>" alt="<?= $product->Name ?>" /></a></td>
                <td><a href="<?= site_url($product->Link) ?>"><?= $product->Sku ?></a></td>
                <td><a href="<?= site_url($product->Link) ?>"><?= $product->Name ?></a></td>
                <td><?= number_format($product->ProductPrice, 2) ?> lei</td>
                <td><a href="<?= site_url($product->Link) ?>" class="btn btn-md btn-danger">Посмотреть</a></td>
            </tr>
        <?php } ?>
        <?php foreach ($products as $product) { ?>
            <tr>
                <td class="prod-thumb"><a class="fancybox" href="<?= base_url('public/uploads/products/' . $product->Image) ?>"><img src="<?= base_url('public/uploads/products/' . $product->Thumb) ?>" alt="<?= $product->Name ?>" /></a></td>
                <td><a href="<?= site_url($product->Link) ?>"><?= $product->Sku ?></a></td>
                <td><a href="<?= site_url($product->Link) ?>"><?= $product->Name ?></a></td>
                <td><?= number_format($product->ProductPrice, 2) ?> lei</td>
                <td><a href="<?= site_url($product->Link) ?>" class="btn btn-md btn-danger">Посмотреть</a></td>
            </tr>
        <?php } ?>
        <?php foreach ($products as $product) { ?>
            <tr>
                <td class="prod-thumb"><a class="fancybox" href="<?= base_url('public/uploads/products/' . $product->Image) ?>"><img src="<?= base_url('public/uploads/products/' . $product->Thumb) ?>" alt="<?= $product->Name ?>" /></a></td>
                <td><a href="<?= site_url($product->Link) ?>"><?= $product->Sku ?></a></td>
                <td><a href="<?= site_url($product->Link) ?>"><?= $product->Name ?></a></td>
                <td><?= number_format($product->ProductPrice, 2) ?> lei</td>
                <td><a href="<?= site_url($product->Link) ?>" class="btn btn-md btn-danger">Посмотреть</a></td>
            </tr>
        <?php } ?>
</tbody>
</table>
<div class="clearfix"></div>
<?php $count_pages = ceil($count / $per_page); ?>
<?php if ($count_pages > 1) { ?>
    <div class="pagination">
        <ul id="products-page" class="category-sort">
            <?php for ($i = 1; $i <= $count_pages; $i++) { ?>
                <li><a class="<?= $page == $i ? 'active' : '' ?>" data="<?= $i ?>"><?= $i ?></a></li>
            <?php } ?>
        </ul>
    </div>
<?php } ?>
<div class="clearfix"></div>