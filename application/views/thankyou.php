<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<section class="content">
    <div class="container">
        <h1 class="text-center"><?= lang('Thankyou') ?></h1>
        <br />
        <div class="row">
            <div class="col-md-offset-3 col-md-6">
                <div class="panel panel-primary text-center">
                    <div class="panel-heading">
                        <h4><?= lang('ThankyouTitle') ?></h4>
                    </div>
                    <div class="panel-body">
                        <h4><?= lang('ThankyouText') ?></h4>
                        <br />
                        <p><b><?= lang('ThankyouText2') ?> <?= $order->ID ?></b></p>
                        <br />
                        <a href="" target="_blank" class="btn btn-info btn-lg"><i class="glyphicon glyphicon-save-file"></i> <?= lang('DownloadInvoice') ?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>