<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<section class="content">
    <div class="container">
        <div class="row">
            <form id="cart-form" method="post">
                <div class="col-md-8 col-md-offset-2">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <div class="panel-title">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <h5><span class="glyphicon glyphicon-shopping-cart"></span> <?= lang('ShoppingCart') ?></h5>
                                    </div>
                                    <div class="col-xs-6">
                                        <a href="<?= site_url() ?>" class="btn btn-primary btn-block">
                                            <span class="glyphicon glyphicon-share-alt"></span> <?= lang('ContinueShopping') ?>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            <?php if (empty($contents)) { ?>
                            <h3 class="text-center"><?= lang('CartEmpty') ?></h3>
                            <?php } else { ?>
                            <?php foreach ($contents as $item) { ?>
                            <div data-product-id="<?= $item['ID'] ?>" data-price="<?= $item['Price'] ?>" class="row product-row">
                                <div class="col-xs-2">
                                    <a class="fancybox" href="<?= base_url('public/uploads/products/' . $item['Image']) ?>">
                                        <img style="height: 80px;" class="img-thumbnail" src="<?= base_url('public/uploads/products/' . $item['Thumb']) ?>">
                                    </a>
                                </div>
                                <div class="col-xs-4">
                                    <h4 class="product-name"><strong><?= $item['Name'] ?></strong></h4>
                                    <h4><small class="text-muted"><?= $item['CategoryName'] ?></small></h4>
                                </div>
                                <div class="col-xs-6">
                                    <div class="col-xs-6 text-right">
                                        <?php if ($this->shopType != 'Angro') { ?>
                                        <h6><strong><?= $item['Price'] ?> MDL <span class="text-muted">&nbsp;&nbsp;x</span></strong></h6>
                                        <?php } ?>
                                    </div>
                                    <div class="col-xs-4">
                                        <input type="number" min="1" step="1" name="Quantity[<?= $item['ID'] ?>]" class="form-control input" value="<?= $item['Quantity'] ?>">
                                    </div>
                                    <div class="col-xs-2">
                                        <button type="button" class="btn btn-link delete-product">
                                            <span class="glyphicon glyphicon-trash text-danger"> </span>
                                        </button>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <hr />
                            </div>
                            <?php } ?>
                            <?php } ?>
                        </div>
                        <?php if (!empty($contents)) { ?>
                        <div class="panel-footer">
                            <div class="row text-center">
                                <div class="col-xs-9">
                                    <?php if ($this->shopType != 'Angro') { ?>
                                    <h4 class="text-right"><?= lang('CartTotal') ?> <strong><span id="cart-total"><?= number_format($total, 2) ?></span> MDL</strong></h4>
                                    <?php } ?>
                                </div>
                                <div class="col-xs-3">
                                    <button type="submit" class="btn btn-success btn-block">
                                        Checkout
                                    </button>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>

<script>
    
    $('#cart-form input[name^=Quantity]').change(function(){
        var row = $(this).closest('.product-row');
        var productID = row.attr('data-product-id');
        var quantity = $(this).val();
        
        $.post('/main/cart_add/1', {productID: productID, quantity: quantity}, function(json){
            notif({
                msg: json.messages,
                type: json.result,
                position: "right"
            });
            $('#cart-amount').text(json.amount);
            $('#cart-total').text(json.amount);
        }, 'json');
    });
    
    $('#cart-form .delete-product').click(function(){
        var row = $(this).closest('.product-row');
        var productID = row.attr('data-product-id');
        $.post('/main/cart_delete', {productID: productID}, function(json){
            notif({
                msg: json.messages,
                type: json.result,
                position: "right"
            });
            $('#cart-amount').text(json.amount);
            $('#cart-total').text(json.amount);
            row.remove();
        }, 'json');
    });
    
</script>