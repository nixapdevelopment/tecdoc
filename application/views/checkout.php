<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<section class="content">
    <div class="container">
        <form class="form-horizontal" method="post" action="">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <!--SHIPPING METHOD-->
                <div class="panel panel-info">
                    <div class="panel-heading"><i class="glyphicon glyphicon-user"></i> <?= lang('UserInfo') ?></div>
                    <div class="panel-body">
                        <div class="form-group">
                            <div class="col-md-12"><strong><?= lang('Name') ?></strong> <span class="text-danger">*</span></div>
                            <div class="col-md-12">
                                <input required type="text" name="name" class="form-control" value="<?= set_value('name', isset($user->Name) ? $user->Name : '') ?>" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12"><strong><?= lang('Region') ?></strong> <span class="text-danger">*</span></div>
                            <div class="col-md-12">
                                <?= form_dropdown('region', $this->config->item('regions'), set_value('region', isset($user->Region) ? $user->Region : 0), 'class="form-control select2" required') ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12"><strong><?= lang('Address') ?></strong> <span class="text-danger">*</span></div>
                            <div class="col-md-12">
                                <input required type="text" name="address" class="form-control" value="<?= set_value('address', isset($user->Address) ? $user->Address : 0) ?>" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12"><strong><?= lang('Zip') ?></strong> <span class="text-danger">*</span></div>
                            <div class="col-md-12">
                                <input required type="text" name="zip" class="form-control" value="<?= set_value('zip', isset($user->Zip) ? $user->Zip : '') ?>" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12"><strong><?= lang('Phone') ?></strong> <span class="text-danger">*</span></div>
                            <div class="col-md-12"><input required type="text" name="phone" class="form-control" value="<?= set_value('phone', isset($user->Phone) ? $user->Phone : '') ?>" /></div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12"><strong><?= lang('Email') ?></strong> <span class="text-danger">*</span></div>
                            <div class="col-md-12"><input required type="email" name="email" class="form-control" value="<?= set_value('email', isset($user->UserName) ? $user->UserName : '') ?>" /></div>
                        </div>
                    </div>
                </div>
                <!--SHIPPING METHOD END-->
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <!--CREDIT CART PAYMENT-->
                <div class="panel panel-info">
                    <div class="panel-heading"><span><i class="glyphicon glyphicon-lock"></i></span> Payment</div>
                    <div class="panel-body">
                        <div>
                            <div class="panel panel-info">
                                <div class="panel-body">
                                    <?php if (empty($contents)) { ?>
                                    <h3 class="text-center"><?= lang('CartEmpty') ?></h3>
                                    <?php } else { ?>
                                    <?php foreach ($contents as $item) { ?>
                                    <div data-product-id="<?= $item['ID'] ?>" data-price="<?= $item['Price'] ?>" class="row product-row">
                                        <div class="col-xs-2">
                                            <a class="fancybox" href="<?= base_url('public/uploads/products/' . $item['Image']) ?>">
                                                <img class="img-thumbnail" src="<?= base_url('public/uploads/products/' . $item['Thumb']) ?>">
                                            </a>
                                        </div>
                                        <div class="col-xs-4">
                                            <h5 class="product-name"><strong><?= $item['Name'] ?></strong></h5>
                                            <h5><small class="text-muted"><?= $item['CategoryName'] ?></small></h5>
                                        </div>
                                        <div class="col-xs-6">
                                            <div class="col-xs-6 text-right">
                                                <h6><strong><?= $item['Price'] ?> MDL <span class="text-muted">x</span></strong></h6>
                                            </div>
                                            <div class="col-xs-6">
                                                <input type="number" min="1" step="1" name="Quantity[<?= $item['ID'] ?>]" class="form-control input" value="<?= $item['Quantity'] ?>">
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <hr />
                                    </div>
                                    <?php } ?>
                                    <?php } ?>
                                </div>
                                <?php if (!empty($contents)) { ?>
                                <div class="panel-footer">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6">
                                            <a href="<?= site_url('cart') ?>" class="btn btn-warning"><i class="glyphicon glyphicon-chevron-left"></i> <?= lang('BackToCart') ?></a>
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <h4 class="text-right"><?= lang('CartTotal') ?> <strong><span id="cart-total"><?= number_format($total, 2) ?></span> MDL</strong></h4>
                                        </div>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                        <br />
                        <label class="control-label"><?= lang('PaymentType') ?></label>
                        <div>
                            <div class="radio">
                                <label>
                                    <input checked type="radio" name="payment" id="optionsRadios2" value="card">
                                    <?= lang('CardPayment') ?>
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="payment" id="optionsRadios2" value="cash">
                                    <?= lang('CashPayment') ?>
                                </label>
                            </div>
                        </div>
                        <br />
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <button type="submit" class="btn btn-primary btn-submit-fix">Place Order</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!--CREDIT CART PAYMENT END-->
            </div>
        </form>
    </div>
</section>