

<section class="socials-home">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h3 class="title-1">Мы на Facebook.com: </h3>
                <a href="" class="go-social-link">Перейти на страницу: facebook.com</a>
            </div>
            <div class="col-md-6">
                <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fnixap&tabs&width=500&height=160&small_header=true&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=293042120875424" width="500" height="160" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
            </div>
        </div>
    </div>
</section>

<footer class="footer">
    <div class="container">
        &copy; USAUTOVAN SRL - Магазин онлайн по поиску авто-запчастей
    </div>
</footer>
<script>
    $(document).on('click', '*[add-to-cart]', function () {
        $.post('/main/cart_add', {productID: $(this).attr('add-to-cart')}, function (json) {
            notif({
                msg: json.messages,
                type: json.result,
                position: "right"
            });
            $('#cart-amount').text(json.amount);
        }, 'json');
    });

    setTimeout(function () {
        $('.alert.alert-success').slideUp(500, function () {
            $(this).remove();
        });
    }, 4000);

    $('.select-2').select2({
        'theme': 'bootstrap'
    });

    $('.datepicker').datepicker({
        format: 'dd.mm.yyyy',
        maxDate: moment(),
        clearBtn: true
    });
    
    $('a.fancybox').fancybox();
    
    $('#search-product-input').select2({
        ajax: {
            url: "/main/product_search",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    q: params.term, // search term
                    page: params.page
                };
            },
            processResults: function (data, params) {
                console.log(data);
                params.page = params.page || 1;
                return {
                    results: data,
                    pagination: {
                        more: (params.page * 30) < data.total_count
                    }
                };
            },
            cache: true
        },
        escapeMarkup: function (markup) { return markup; },
        minimumInputLength: 3
    }).on("select2:selecting", function(e) { 
        window.location.href = e.params.args.data.id;
    });
</script>
</body>
</html>