<!DOCTYPE html>
<html style="overflow-y: scroll;">
    <head>
        <meta charset="UTF-8">
        <title>ADMIN: AGM</title>
        <link rel="stylesheet" href="<?= base_url('public/css/styles.css') ?>">
        <link rel="stylesheet" href="<?= base_url('public/css/admin_styles.css') ?>">
        <link rel="stylesheet" href="<?= base_url('public/css/bootstrap.css') ?>">
        <link rel="stylesheet" href="<?= base_url('public/css/bootstrap-theme.min.css') ?>">
        <link rel="stylesheet" href="<?= base_url('public/css/font-awesome.min.css') ?>">
        <link rel="stylesheet" href="<?= base_url('public/js/fancybox/jquery.fancybox.css') ?>">
        <link rel="stylesheet" href="<?= base_url('public/css/easyTree.min.css') ?>">
        <link rel="stylesheet" href="<?= base_url('public/css/notifIt.css') ?>">
        <link rel="stylesheet" href="<?= base_url('public/css/select2.min.css') ?>">
        
        <script>var LOADER = '<div class="text-center"><img src="<?= site_url('public/images/preloader.gif') ?>" /></div>';</script>
        <script>var SITE_LANG = '<?= $this->config->item('languages')[$this->langID]['Slug'] ?>';</script>
        <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
        <script src="<?= base_url('public/js/bootstrap.min.js') ?>"></script>
        <script src="<?= base_url('public/js/fancybox/jquery.fancybox.js') ?>"></script>
        <script src="<?= base_url('public/js/fancybox/jquery.fancybox.pack.js') ?>"></script>        
        <script src="<?= base_url('public/js/easyTree.min.js') ?>"></script>
        <script src="<?= base_url('public/js/validate.js') ?>"></script>
        <script src="<?= base_url('public/js/notifIt.min.js') ?>"></script>
        <script src="<?= base_url('public/js/select2.min.js') ?>"></script>
        <script src="<?= base_url('public/js/bootstrap-datepicker.js') ?>"></script>
        <script src="<?= base_url('public/js/ckeditor/ckeditor.js') ?>"></script>
        <script src="<?= base_url('public/js/jquery.tmpl.min.js') ?>"></script>
        <script src="<?= base_url('public/js/moment.js') ?>"></script>
    </head>
    <body class="admin-panel">
        <header class="header">
            <div class="top-bar">
                <div class="container">
                    <div class="row">
                        <div class="left-top-bar col-md-4">
                            <?= lang('Wellcome') ?>, <?= $this->_user->Name ?>
                        </div>
                        <div class="right-top-bar col-md-8">
                            <div class="lang-bar dropdown">
                                <a href="" id="change-lang" data-toggle="dropdown"><?= lang('AdminAccount') ?></a>
                                <ul class="dropdown-menu pull-right" aria-labelledby="change-lang">
                                    <li><a href="<?= site_url('admin/profile-settings') ?>"><?= lang('ProfileSettings') ?></a></li>
                                    <li><a href="<?= site_url('logout') ?>"><?= lang('Logout') ?></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="header-bar">
                    <div class="row">
                        <div class="header-logo col-md-3">
                            <a href="/">
                                <img src="<?= base_url('public/images/logo.png') ?>" alt="">
                                <span>Succesul nostru este în calitatea produselor!</span>
                            </a>
                        </div>
                        <div class="col-md-4"></div>
                        <div class="col-md-2"></div>
                        <div class="cart-bar col-md-3">
                            <a class="user-block" href="#">
                                <i class="fa fa-shopping-basket"></i>
                                <span>Comenzi noi (2)</span>                               
                            </a>
                            <a class="cart-block" href="">
                                <i class="fa fa-users"></i>
                                <span>Conturi (manageri)</span>
                            </a>
                        </div>
                    </div>
                </div>
                <nav class="main-menu">
                    <ul>
                        <li><a href="<?= site_url('admin'); ?>">Dashboard</a></li>
                        <li><a href="<?= site_url('admin/categories'); ?>"><?= lang('Categories') ?></a></li>
                        <li><a href="<?= site_url('admin/products'); ?>"><?= lang('Products') ?></a></li>
                        <li><a href="<?= site_url('admin/filters'); ?>"><?= lang('Filters') ?></a></li>
                        <li><a href="">Clienți</a></li>
                        <li><a href="<?= site_url('admin/news'); ?>">Noutăți</a></li>
                        <li><a href="<?= site_url('admin/pages'); ?>">Pagini</a></li>
                        <li><a href="<?= site_url('admin/orders'); ?>">Comenzii</a></li>
                        <li></li>
                    </ul>
                </nav>
            </div>
        </header>
        <div class="container">
            <div class="sub-header">
                <div class="row">
                    <div class="col-md-12">
                        <?= breadscrumbs($this->breadscrumbs) ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade bs-example-modal-xs" id="panou-client" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-xs" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Panou client</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <h4>Intră în cont</h4>
                                <form>
                                    <div class="form-group">
                                        <input type="email" class="form-control" placeholder="Email">
                                    </div>
                                    <div class="form-group">
                                        <input type="password" class="form-control" placeholder="Parola">
                                    </div>
                                    <div class="form-group text-center">
                                        <p><a href="">Ai uitat parola?</a></p>
                                        <p><button type="submit" class="btn btn-default">Intră</button></p>
                                        <p>sau <a href="">Înscrie-te</a></p>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>