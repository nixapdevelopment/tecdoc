<!DOCTYPE html>
<html style="overflow-y: scroll;">
    <head>
        <meta charset="UTF-8">
        <title>US Autovan</title>
        <link rel="stylesheet" href="<?= base_url('public/css/styles.css') ?>">
        <link rel="stylesheet" href="<?= base_url('public/css/admin_styles.css') ?>">
        <link rel="stylesheet" href="<?= base_url('public/css/bootstrap.css') ?>">
        <link rel="stylesheet" href="<?= base_url('public/css/bootstrap-theme.min.css') ?>">
        <link rel="stylesheet" href="<?= base_url('public/css/font-awesome.min.css') ?>">
        <link rel="stylesheet" href="<?= base_url('public/js/fancybox/jquery.fancybox.css') ?>">
        <link rel="stylesheet" href="<?= base_url('public/css/easyTree.min.css') ?>">
        <link rel="stylesheet" href="<?= base_url('public/css/notifIt.css') ?>">
        <link rel="stylesheet" href="<?= base_url('public/css/select2.min.css') ?>">
        <link rel="stylesheet" href="<?= base_url('public/css/bootstrap-slider.min.css') ?>">
	

        <script>var LOADER = '<div class="text-center"><img src="<?= site_url('public/images/preloader.gif') ?>" /></div>';</script>
        <script>var SITE_LANG = '<?= $this->config->item('languages')[$this->langID]['Slug'] ?>';</script>
        <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
        <script src="<?= base_url('public/js/bootstrap.min.js') ?>"></script>
        <script src="<?= base_url('public/js/fancybox/jquery.fancybox.js') ?>"></script>
        <script src="<?= base_url('public/js/fancybox/jquery.fancybox.pack.js') ?>"></script>        
        <script src="<?= base_url('public/js/easyTree.min.js') ?>"></script>
        <script src="<?= base_url('public/js/validate.js') ?>"></script>
        <script src="<?= base_url('public/js/notifIt.min.js') ?>"></script>
        <script src="<?= base_url('public/js/select2.min.js') ?>"></script>
        <script src="<?= base_url('public/js/bootstrap-datepicker.js') ?>"></script>
        <script src="<?= base_url('public/js/ckeditor/ckeditor.js') ?>"></script>
        <script src="<?= base_url('public/js/jquery.tmpl.min.js') ?>"></script>
        <script src="<?= base_url('public/js/moment.js') ?>"></script>
        <script src="<?= base_url('public/js/bootstrap-slider.js') ?>"></script>
	<link rel="stylesheet" href="<?= base_url('public/js/owl-carousel/owl.carousel.css') ?>">
	<link rel="stylesheet" href="<?= base_url('public/js/owl-carousel/owl.theme.css') ?>">
	<link rel="stylesheet" href="<?= base_url('public/js/owl-carousel/owl.transitions.css') ?>">
	
	<script src="<?= base_url('public/js/owl-carousel/owl.carousel.js') ?>"></script>	
	
	<script src="<?= base_url('public/js/scripts.js') ?>"></script>

        <script>
            $(document).ready(function () {
                var elementPosition = $('#nav-menu').offset();

                $(window).scroll(function () {
                    if ($(window).scrollTop() > elementPosition.top) {
                        $('#nav-menu').css('position', 'fixed').css('top', '0');
                        $('#nav-menu').css('margin', '0');
                    } else {
                        $('#nav-menu').css('position', 'static');
                    }
                });
            });
        </script>
        
    </head>
    <body class="admin-panel">
        <div class="top-bar">
            <div class="container">
                <div class="row">
                    <div class="top-contact col-md-2">
                        <i class="fa fa-phone"></i> <a href="">+373 60 123-456</a>
                    </div>
                    <div class="top-contact col-md-3">
                        <i class="fa fa-envelope-o"></i> <a href="">office@usautovan.md</a>
                    </div>                                
                    <div class="top-contact pull-right col-md-3">
                        <?php if (!empty($this->_user)) { ?>
                            <a href="<?= site_url($this->accountLink) ?>"><?= lang('UserAccount') ?></a> | 
                            <a href="<?= site_url('logout') ?>"><?= lang('Logout') ?></a>
                        <?php } else { ?>
                            <a href="<?= site_url('login') ?>"><?= lang('Login') ?></a> | 
                            <a href="<?= site_url('registration') ?>"><?= lang('Registration') ?></a>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>       

        <header class="header">
            <div class="container header-inn">
                <div class="row">
                    <div class="col-md-3">
                        <a href="/">
                            <img class="header-logo" src="<?= base_url('public/images/logo.png') ?>" alt="" />
                        </a>
                    </div>
                    <div class="col-md-3">
                        <div class="header-item">
                            <div class="header-item-ico">
                                <i class="fa fa-map-marker"></i>
                            </div>
                            <div class="header-item-text">
                                <p>Наш адрес</p>
                                <strong>ул. Бариера Скулень 9А</strong>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="header-item">
                            <div class="header-item-ico">
                                <i class="fa fa-clock-o"></i>
                            </div>
                            <div class="header-item-text">
                                <p>График работы</p>
                                <strong>Пн - Пт, 9-18</strong>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="header-item-phone">
                            <p><i class=""></i> Свяжитесь с нами!</p>
                            <a href="">069 244 677</a>
                        </div>	
                    </div>
                </div>
            </div>
            <nav class="menu" id="nav-menu">
                <div class="container">
                    <div class="row">
                        <?php if ($this->menuType == 'Admin') { ?>
                            <ul class="col-md-12">
                                <?= menu($this->menu[$this->menuType]) ?>
                            </ul>
                            
                        <?php } else { ?>
                            
                          
                        <ul class="col-md-6">
                            <?= menu($this->menu[$this->menuType]) ?>
                        </ul>
                        <div class="col-md-3">
                            <div id="cart-fly" class="menu-cart">
                                <a class="header-cart" href="<?= site_url('cart') ?>">
                                    <i class="fa fa-shopping-cart"></i>
                                    <p>Корзина: <span id="cart-amount"><?= number_format($this->cartAmount, 2) ?></span></p>
                                </a>
                            </div>
                        </div>
                        <form class="col-md-3 slider-search">
                            <select style="width: 86%;" id="search-product-input"  name="sku" placeholder="Поиск по сайту ...">
                                <option>Поиск по сайту ...</option>
                            </select>
                            <button type="submit"><i class="fa fa-search"></i></button>
                        </form>  
                       <?php } ?>
                    </div>
                </div>
            </nav>
        </header>

        <div class="container hidden">
            <div class="sub-header">
                <div class="row">
                    <div class="col-md-12">
                        <?= breadscrumbs($this->breadscrumbs) ?>
                    </div>
                </div>
            </div>
        </div>