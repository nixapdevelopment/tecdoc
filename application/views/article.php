<section class="hot-home">
    <div class="container">
        <h1><?= $article->Title ?></h1>
        <div>
            <?= $article->Text ?>
        </div>
        <?php if (count($images) > 0) { ?>
        <br />
        <div class="row">
            <?php foreach ($images as $img) { ?>
            <div class="col-md-3">
                <a class="fancybox" rel="article" href="<?= site_url('public/uploads/news/' . $img->Image) ?>">
                    <img class="img-thumbnail" src="<?= site_url('public/uploads/news/' . $img->Thumb) ?>" />
                </a>
            </div>
            <?php } ?>
        </div>
        <?php } ?>
        <?php if (count($files) > 0) { ?>
        <hr />
        <div class="row">
            <?php foreach ($files as $file) { ?>
            <div class="col-md-3 text-center">
                <a target="_blank" href="<?= site_url('public/uploads/news/' . $file->Path) ?>">
                    <?= $file->Name ?>
                </a>
            </div>
            <?php } ?>
        </div>
        <?php } ?>
    </div>
</section>