<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<section class="slideshow">
    <div class="container">
        <div class="row">
            
            <div class="col-md-9">
                <div id="home-slide-1" class="owl-carousel">					
                    <a class="slideshow-item item" href="">
                        <img src="<?= base_url('public/images/banner1.png'); ?>" />
                    </a>				
                    <a class="slideshow-item item" href="">
                        <img src="<?= base_url('public/images/banner1.png'); ?>" />
                    </a>

                </div>
            </div>
            <div class="col-md-3 banner-2">	
                <div id="home-slide-2" class="col-md-3 owl-carousel banner-2">	
                    <div class="item">
                        <img src="<?= base_url('public/images/banner2.png'); ?>" />
                    </div>
                    <div class="item">
                        <img src="<?= base_url('public/images/banner2.png'); ?>" />
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-4">
                <div class="inn-text">
                    <i class="fa fa-paper-plane"></i>
                    Доставка по всей Молдове
                </div>
            </div>
            <div class="col-md-4">
                <div class="inn-text">
                    <i class="fa fa-certificate"></i>
                    Качество 100%
                </div>
            </div>
            <div class="col-md-4">
                <div class="inn-text">
                    <i class="fa fa-clock-o"></i>
                    Работаем с Пн-Вс, 9-18
                </div>
            </div>
        </div>
    </div>
</section>

<section class="search-home">
    <div class="container">
        <h3 class="title-1">Поиск запчастей по каталогу</h3>
        <div class="">
            <div class="col-md-12 auto-select">

                <a href=""><img src="<?= base_url('public/images/autos/cadd.jpg'); ?>" /> Cadillac</a>
                <a href=""><img src="<?= base_url('public/images/autos/602.png'); ?>" /> Chevrolet</a>					
                <a href=""><img src="<?= base_url('public/images/autos/513.png'); ?>" /> Chrysler</a>
                <a href=""><img src="<?= base_url('public/images/autos/521.png'); ?>" /> Dodge</a>
                <a href=""><img src="<?= base_url('public/images/autos/525.png'); ?>" /> Ford</a>
                <a href=""><img src="<?= base_url('public/images/autos/gmc.jpg'); ?>" /> GMC</a>
                <a href=""><img src="<?= base_url('public/images/autos/jee.jpg'); ?>" /> Jeep</a>
                <a href=""><img src="<?= base_url('public/images/autos/lin.jpg'); ?>" /> Lincoln</a>





                <div class="collapse" id="all_makes">			

                    <a href=""><img src="<?= base_url('public/images/autos/502.png'); ?>" /> Alfa Romeo</a>
                    <a href=""><img src="<?= base_url('public/images/autos/504.png'); ?>" /> Audi</a>
                    <a href=""><img src="<?= base_url('public/images/autos/511.png'); ?>" /> BMW</a>
                    <a href=""><img src="<?= base_url('public/images/autos/603.png'); ?>" /> Dacia</a>
                    <a href=""><img src="<?= base_url('public/images/autos/517.png'); ?>" /> Daihatsu</a>
                    <a href=""><img src="<?= base_url('public/images/autos/649.png'); ?>" /> Daewoo</a>
                    <a href=""><img src="<?= base_url('public/images/autos/524.png'); ?>" /> Fiat</a>

                    <a href=""><img src="<?= base_url('public/images/autos/533.png'); ?>" /> Honda</a>	

                    <a href=""><img src="<?= base_url('public/images/autos/538.png'); ?>" /> Isuzu</a>
                    <a href=""><img src="<?= base_url('public/images/autos/539.png'); ?>" /> Iveco</a>
                    <a href=""><img src="<?= base_url('public/images/autos/540.png'); ?>" /> Jaguar</a>
                    <a href=""><img src="<?= base_url('public/images/autos/648.png'); ?>" /> KIA</a>
                    <a href=""><img src="<?= base_url('public/images/autos/545.png'); ?>" /> Lada</a>
                    <a href=""><img src="<?= base_url('public/images/autos/546.png'); ?>" /> Lancia</a>
                    <a href=""><img src="<?= base_url('public/images/autos/551.png'); ?>" /> Man</a>

                    <a href=""><img src="<?= base_url('public/images/autos/552.png'); ?>" /> Mazda</a>
                    <a href=""><img src="<?= base_url('public/images/autos/553.png'); ?>" /> Mercedes-Benz</a>
                    <a href=""><img src="<?= base_url('public/images/autos/555.png'); ?>" /> Mitsubishi</a>
                    <a href=""><img src="<?= base_url('public/images/autos/558.png'); ?>" /> Nissan</a>

                    <a href=""><img src="<?= base_url('public/images/autos/561.png'); ?>" /> Opel</a>
                    <a href=""><img src="<?= base_url('public/images/autos/563.png'); ?>" /> Peugeout</a>
                    <a href=""><img src="<?= base_url('public/images/autos/565.png'); ?>" /> Porsche</a>
                    <a href=""><img src="<?= base_url('public/images/autos/739.png'); ?>" /> Renault</a>
                    <a href=""><img src="<?= base_url('public/images/autos/568.png'); ?>" /> Rover</a>

                    <a href=""><img src="<?= base_url('public/images/autos/569.png'); ?>" /> Saab</a>
                    <a href=""><img src="<?= base_url('public/images/autos/572.png'); ?>" /> Scania</a>
                    <a href=""><img src="<?= base_url('public/images/autos/573.png'); ?>" /> Seat</a>
                    <a href=""><img src="<?= base_url('public/images/autos/575.png'); ?>" /> Skoda</a>

                    <a href=""><img src="<?= base_url('public/images/autos/576.png'); ?>" /> Subaru</a>
                    <a href=""><img src="<?= base_url('public/images/autos/577.png'); ?>" /> Suzuki</a>
                    <a href=""><img src="<?= base_url('public/images/autos/579.png'); ?>" /> Toyota</a>
                    <a href=""><img src="<?= base_url('public/images/autos/586.png'); ?>" /> Volvo</a>

                    <a href=""><img src="<?= base_url('public/images/autos/587.png'); ?>" /> Volkswagen</a>
                    <a href=""><img src="<?= base_url('public/images/autos/647.png'); ?>" /> Hyundai</a>

                </div>

                <button type="button" data-toggle="collapse" class="btn btn-danger" data-target="#all_makes">Открыть / Скрыть все марки</button>
            </div>
        </div>
    </div>
</section>

<section class="tabs-home">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <ul class="tabs-home-menu">
                    <li role="presentation" class="active"><a href="#short-desc" role="tab" data-toggle="tab">Коротко о нас</a></li>
                    <li role="presentation"><a href="#why-us" role="tab" data-toggle="tab">Почему мы?</a></li>
                    <li role="presentation"><a href="#car-service" role="tab" data-toggle="tab">Автосервис</a></li>

                </ul>
            </div>
            <div class="col-md-9">
                <div class="tabs-home-inn tab-content">
                    <div role="tabpanel" class="tab-pane active" id="short-desc">
                        <h4>USAUTOVAN SRL - Магазин оригинальных запчастей в Кишиневе</h4>
                        <p>
                            В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.
                        </p>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="why-us">
                        <h4>Почему мы?</h4>
                        <p>
                            Lorem Ipsum не только успешно пережил без заметных изменений пять веков. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.
                        </p>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="car-service">
                        <h4>Автосервис</h4>
                        <p>
                            Его популяризации в новое время послужили публикация листов Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="clearfix"></div>

<section class="hot-home">
    <div class="container">
        <h3 class="title-1">Наши товары</h3>
       
            <?= $this->data['categories'] ?>
        
    </div>
</section>

<div class="clearfix"></div>

<?php if (count($news) > 0) { ?>
<section class="news-home">
    <div class="container">
        <h3 class="title-1">Новости <small class="pull-right">посмотреть <a class="cat-go" href="<?= site_url('news') ?>">все новости</a></small></h3>
        <div class="row">
            <?php foreach ($news as $item) { ?>
            <article class="news-item col-md-6">
                <div class="news-item-inn">
                    <time><?= date('d.m.Y', strtotime($item->Date)) ?></time>
                    <a href="<?= site_url($item->Link) ?>"><?= $item->Title ?></a>
                </div>
            </article>
            <?php } ?>
        </div>
    </div>
</section>
<?php } ?>