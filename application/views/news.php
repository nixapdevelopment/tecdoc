<section class="hot-home">
    <div class="container">
        <?php foreach ($news as $item) { ?>
        <div>
            <h2><?= $item->Title ?></h2>
            <div>
                <?= substr(strip_tags($item->Text), 0, 200) ?>... <a href="<?= site_url($item->Link) ?>"><?= lang('More') ?> ></a>
            </div>
            <div><?= date('d.m.Y', strtotime($item->Date)) ?></div>
        </div>
        <?php } ?>
        <div>
            <?= $pagination ?>
        </div>
    </div>
</section>