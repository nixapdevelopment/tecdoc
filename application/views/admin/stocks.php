<div class="container">
    <h1><?= lang('Stocks') ?></h1>
    <div>
        <a class="btn btn-success" href="<?= site_url('admin/stock') ?>"><i class="fa fa-plus-circle"></i> Adauga stocuri</a>&nbsp;&nbsp;
        <a class="btn btn-primary" href="<?= site_url('admin/edit_order') ?>"><i class="fa fa-cart-plus"></i> Adauga comanda</a>
    </div>
    <hr />
    <div>
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Date</th>
                    <th>Type</th>
                    <th></th>
                    <th style="width: 220px;"></th>
                </tr>
            </thead>
            <tbody>
                <form>
                    <tr>
                        <td></td>
                        <td><input type="text" name="Date" value="<?= $this->input->get('Date') ?>" onkeypress="return false" class="form-control datepicker" /></td>
                        <td>
                            <?= form_dropdown('Type', [
                                '' => 'All',
                                'Income' => 'Income',
                                'Sale' => 'Sale'
                            ], $this->input->get('Type'), 'class="form-control"') ?>
                        </td>
                        <td></td>
                        <td>
                            <button type="submit" class="btn btn-primary"><i class="fa fa-filter"></i> Filtreza</button>
                            <a href="<?= site_url('admin/stocks') ?>" class="btn btn-danger">Reseta filtre</a>
                        </td>
                    </tr>
                </form>
                <?php foreach ($stocks as $item) { ?>
                <tr>
                    <td><?= $item['data']->OperationID ?></td>
                    <td><?= date('d.m.Y H:i:s', strtotime($item['data']->Date)) ?></td>
                    <td><?= $item['data']->Type ?></td>
                    <td><button data-toggle="popover" title="Info" data-content="<?= implode('<br>', $item['ps']) ?>">Info</button></td>
                    <td class="text-center"><a href="<?= site_url('admin/stock/' . $item['data']->OperationID) ?>"><i class="fa fa-eye"></i></a></td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>
<script>

    $(document).ready(function(){
        $('*[data-toggle="popover"]').popover({
            html : true
        });
    });

</script>