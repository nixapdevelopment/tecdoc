<div class="container">
    <h1><?= lang('Stocks') ?></h1>
    <div>
        <form onsubmit="if ($('select[name^=ProductID]').length == 0) { alert('Добавьте продукты в заказ'); return false; }" id="edit-order-form" class="form-horizontal" method="post">
            <div>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Client</a></li>
                    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Produse</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <br />
                    <div role="tabpanel" class="tab-pane active" id="home">
                        <div class="panel panel-info">
                            <div class="panel-heading"><i class="glyphicon glyphicon-user"></i> <?= lang('UserInfo') ?></div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <div class="col-md-12"><strong>Client existent</strong></div>
                                    <div class="col-md-12">
                                        <?= form_dropdown('ClientID', $clients, set_value('name', isset($order_details->ID) ? $order_details->ID : ''), 'class="form-control select-2"') ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12"><strong><?= lang('Name') ?></strong> <span class="text-danger">*</span></div>
                                    <div class="col-md-12">
                                        <input required type="text" name="name" class="form-control" value="<?= set_value('name', isset($order_details->Name) ? $order_details->Name : '') ?>" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12"><strong><?= lang('Region') ?></strong> <span class="text-danger">*</span></div>
                                    <div class="col-md-12">
                                        <?= form_dropdown('region', $this->config->item('regions'), set_value('region', isset($order_details->Region) ? $order_details->Region : 0), 'class="form-control select-2" required') ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12"><strong><?= lang('Address') ?></strong> <span class="text-danger">*</span></div>
                                    <div class="col-md-12">
                                        <input required type="text" name="address" class="form-control" value="<?= set_value('address', isset($order_details->Address) ? $order_details->Address : '') ?>" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12"><strong><?= lang('Zip') ?></strong> <span class="text-danger">*</span></div>
                                    <div class="col-md-12">
                                        <input required type="text" name="zip" class="form-control" value="<?= set_value('zip', isset($order_details->Zip) ? $order_details->Zip : '') ?>" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12"><strong><?= lang('Phone') ?></strong> <span class="text-danger">*</span></div>
                                    <div class="col-md-12"><input required type="text" name="phone" class="form-control" value="<?= set_value('phone', isset($order_details->Phone) ? $order_details->Phone : '') ?>" /></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12"><strong><?= lang('Email') ?></strong> <span class="text-danger">*</span></div>
                                    <div class="col-md-12"><input required type="email" name="email" class="form-control" value="<?= set_value('email', isset($order_details->Email) ? $order_details->Email : '') ?>" /></div>
                                </div>
                                <label class="control-label"><?= lang('PaymentType') ?></label>
                                <div>
                                    <div class="radio">
                                        <label>
                                            <input checked type="radio" name="payment" id="optionsRadios2" value="card">
                                            <?= lang('CardPayment') ?>
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="payment" id="optionsRadios2" value="cash">
                                            <?= lang('CashPayment') ?>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="profile">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <i class="glyphicon glyphicon-shopping-cart"></i> <?= lang('Products') ?>
                            </div>
                            <div class="panel-body">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>Categorie</th>
                                            <th>Produs</th>
                                            <th>Cantitate</th>
                                            <th>Pret</th>
                                            <th>Subtotal</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody id="order-products">
                                        <?php foreach ($order_products as $op) { ?>
                                            <tr>
                                                <td style="width:25%;"><?= form_dropdown('CategoryID[]', $categories, $op->CategoryID, 'class="form-control select-2" required style="width: 100%"') ?></td>
                                                <td style="width:35%;"><?= form_dropdown('ProductID[]', $products, $op->ProductID, 'class="form-control select-2" required style="width: 100%"') ?></td>
                                                <td><input min="1" class="form-control" type="number" name="Quantity[]" value="<?= isset($op->Quantity) ? $op->Quantity : '' ?>" required /></td>
                                                <td><input step="0.01" class="form-control" type="number" name="Price[]" value="<?= isset($op->Price) ? $op->Price : '' ?>" required /></td>
                                                <td><input step="0.01" class="form-control" type="number" name="Subtotal[]" value="<?= isset($op->Price) ? $op->Price * $op->Quantity : 0 ?>" readonly /></td>
                                                <td><button onclick="$(this).closest('tr').remove();" class="btn btn-danger"><i class="fa fa-trash"></i></button></td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                                <hr />
                                <button id="add-order-product" type="button" class="btn btn-primary"><i class="fa fa-plus-circle"></i> Adauga produs</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <button type="submit" class="btn btn-success btn-lg"><i class="fa fa-save"></i> <?= lang('Save') ?></button>
            </div>
        </form>
    </div>
</div>
<script id="order-product-tmpl" type="text/x-jquery-tmpl">
    <tr>
        <td style="width:25%;"><?= form_dropdown('CategoryID[]', $categories, '', 'class="form-control select-2" required') ?></td>
        <td style="width:35%;" class="prod-input"><?= form_dropdown('ProductID[]', ['' => 'Selectati produs'], '', 'class="form-control select-2" required') ?></td>
        <td><input min="1" class="form-control" type="number" name="Quantity[]" value="1" required /></td>
        <td><input step="0.01" class="form-control" type="number" name="Price[]" value="" required /></td>
        <td><input step="0.01" class="form-control" type="number" name="Subtotal[]" value="0.00" readonly /></td>
        <td><button onclick="$(this).closest('tr').remove();" class="btn btn-danger"><i class="fa fa-trash"></i></button></td>
    </tr>
</script>
<script>

    $('#add-order-product').click(function () {
        $('#order-product-tmpl').tmpl().appendTo('#order-products');
        $('#order-products select[name^=CategoryID]:last').select2({
            theme: 'bootstrap'
        }).on("select2:select", function (e) {
            var that = this;
            $.post('/admin/get_product_by_category', {id: e.params.data.id}, function (html) {
                var row = $(that).closest('tr');
                row.find('.prod-input').html(html);
                row.find('.prod-input select').select2({
                    theme: 'bootstrap'
                }).on("select2:select", function (e){
                     $.post('/admin/get_product_price', {id: e.params.data.id}, function (html) {
                         row.find('input[name^=Price]').val(html).trigger('change');
                     });
                });
            });
        });
    });
    
    $('#order-products').on('change', 'input[name^=Price], input[name^=Quantity]', function(){
        calcRow($(this).closest('tr'));
    });
    
    function calcRow(row)
    {
        var qty = row.find('input[name^=Quantity]').val();
        var price = row.find('input[name^=Price]').val();
        row.find('input[name^=Subtotal]').val(qty * price);
    }
    
    $('form select[name=ClientID]').select2({
        theme: 'bootstrap'
    }).on('select2:select', function(e){
        $.post('/admin/get_user', {id: e.params.data.id}, function(user){
            $('input[name=name]').val(user.Name);
            $('input[name=address]').val(user.Address);
            $('input[name=zip]').val(user.Zip);
            $('input[name=phone]').val(user.Phone);
            $('input[name=email]').val(user.UserName);
            $('select[name=region]').val(user.Region).trigger('change');
        }, 'json');
    });

</script>