<div class="container">
    <div class="in-admin">
        <div class="row">
            <div class="col-md-6">
                <h3 class="upper-case-title"><?= lang('Products') ?></h3>
            </div>
            <div class="col-md-6 text-right">
                <a id="add-filter-btn" href="<?= site_url('admin/edit_product') ?>" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> <?= lang('AddProduct') ?></a>
            </div>
        </div>
    </div>
    <div>
        <form id="search-product-form">
            <div class="row">
                <div class="col-md-2">
                    <div class="form-group">
                        <label class="control-label"><?= lang('SKU') ?></label>
                        <input type="text" name="Sku" value="<?= $this->input->get('Sku') ?>" class="form-control" />
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label class="control-label"><?= lang('Categories') ?></label>
                        <?= form_dropdown('Category[]', $all_categories, $this->input->get('Category[]'), 'class="form-control select2" multiple') ?>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label class="control-label"><?= lang('ProductName') ?></label>
                        <input type="text" name="Name" value="<?= $this->input->get('Name') ?>" class="form-control" />
                    </div>
                </div>
                <div class="col-md-1">
                    <div class="form-group">
                        <label class="control-label"><?= lang('Sorting') ?></label>
                        <?= form_dropdown('Sort', [
                            '0' => '-',
                            'az' => 'A-Z',
                            'za' => 'Z-A',
                            'price_d' => lang('Price') . ' &darr;',
                            'price_a' => lang('Price') . ' &uarr;',
                        ], $this->input->get('Sort'), 'class="form-control"') ?>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label class="control-label">&nbsp;</label>
                        <div class="row">
                            <div class="col-sm-2"><input style="height: 34px; width: 34px; position: relative; top: -4px;" type="checkbox" name="IsPromo" /></div>
                            <div class="col-sm-10" style="line-height: 32px; font-size: 18px;"><?= lang('IsPromo') ?></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-1">
                    <div class="form-group">
                        <label class="control-label"><a href="<?= site_url('admin/products') ?>"><?= lang('ClearFilters') ?></a></label>
                        <button class="btn btn-info btn-block"><?= lang('Filter') ?></button>
                    </div>
                </div>
            </div>
        </form>
        <hr />
        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th><input type="checkbox" name="select_all" /></th>
                    <th><?= lang('Image') ?></th>
                    <th><?= lang('SKU') ?></th>
                    <th><?= lang('ProductName') ?></th>
                    <th><?= lang('Price') ?></th>
                    <th><?= lang('Stock') ?></th>
                    <th><?= lang('IsPromo') ?></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($products as $product) { ?>
                <tr>
                    <td><input type="checkbox" name="select_one" /></td>
                    <td></td>
                    <td><?= $product->Sku ?></td>
                    <td><?= $product->Name ?></td>
                    <td><?= $product->ProductPrice ?></td>
                    <td><input type="number" step="1" min="0" value="<?= (float)$product->Stock ?>" /></td>
                    <td><?= $product->IsPromo ? '' : '' ?></td>
                    <td>
                        <a href=""><i class="fa fa-eye text-info"></i></a>
                        <a href="<?= site_url('admin/edit_product', ['id' => $product->ID]) ?>"><i class="fa fa-edit text-success"></i></a>
                        <a href="#"><i class="fa fa-trash text-danger"></i></a>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>

<script>

    $('.select2').select2({
        theme: 'bootstrap'
    });

</script>