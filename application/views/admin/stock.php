<div class="container">
    <h1><?= lang('Stocks') ?></h1>
    <a class="btn btn-info" href="<?= site_url('admin/stocks') ?>"><i class="fa fa-chevron-left"></i> ÎNAPOI LA LISTA</a>
    <hr />
    <div>
        <form method="post">    
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label">Data</label>
                        <input type="text" class="datepicker form-control" value="<?= set_value('Date', isset($operation->Date) ? date('d.m.Y H:i:s', strtotime($operation->Date)) : date('d.m.Y H:i:s')) ?>" />
                    </div>
                </div>
                <div class="col-md-6">
                    <label class="control-label">Tip</label>
                    <select class="form-control" name="Type">
                        <option value="Income">Income</option>
                    </select>
                </div>
            </div>
            <div>
                <button type="button" id="add-ps" class="btn btn-primary"><i class="fa fa-plus"></i> Adauga produse</button>
                <?php  if (isset($operation->Type) && $operation->Type != 'Sale') { ?>
                <a target="_blank" href="<?= site_url('admin/order/' . $operation->OrderID) ?>" class="btn btn-primary">Vezi comanda</a>
                <?php } ?>
                <hr />
            </div>
            <div class="row">
                <div class="col-md-2">
                    <label class="control-label">Categorie</label>
                </div>
                <div class="col-md-3">
                    <label class="control-label">Produs</label>
                </div>
                <div class="col-md-2">
                    <label class="control-label">Cantitate</label>
                </div>
                <div class="col-md-2">
                    <label class="control-label">Sold</label>
                </div>
                <div class="col-md-2">
                    <label class="control-label">Pret</label>
                </div>
            </div>
            <div id="products-wrap" style="min-height: 100px;">
                <?php foreach ($pstocks as $ps) { ?>
                <div class="row">
                    <div class="col-md-2">
                        <input type="hidden" name="psID[]" value="<?= $ps->ID ?>" />
                        <?= form_dropdown('CategoryID[]', $categories, $ps->CategoryID, 'class="form-control select-2" required') ?>
                    </div>
                    <div class="col-md-3 prod-input">
                        <?= form_dropdown('ProductID[]', $products, $ps->ProductID, 'class="form-control select-2" required') ?>
                    </div>
                    <div class="col-md-2">
                        <input class="form-control" type="number" name="Quantity[]" value="<?= isset($ps->Quantity) ? $ps->Quantity : '' ?>" required />
                    </div>
                    <div class="col-md-2">
                        <input class="form-control" type="number" name="Sold[]" value="<?= isset($ps->Sold) ? $ps->Sold : 0 ?>" required />
                    </div>
                    <div class="col-md-2">
                        <input step="0.01" class="form-control" type="number" name="Price[]" value="<?= isset($ps->Price) ? $ps->Price : '' ?>" required />
                    </div>
                    <div class="col-md-1">
                        <button onclick="$(this).closest('.row').remove();" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                    </div>
                </div>
                <?php } ?>
            </div>
            <div>
                <button class="btn btn-success" type="submit"><i class="fa fa-save"></i> Salvare</button>
            </div>
        </form>
    </div>
</div>
<script id="ps-row-tmpl" type="text/x-jquery-tmpl">
    <div class="row">
        <div class="col-md-2">
            <?= form_dropdown('CategoryID[]', $categories, [], 'class="form-control select-2" required') ?>
        </div>
        <div class="col-md-3 prod-input">
            <?= form_dropdown('ProductID[]', ['' => 'Selectati produs'], [], 'class="form-control select-2" required') ?>
        </div>
        <div class="col-md-2">
            <input class="form-control" type="number" name="Quantity[]" required />
        </div>
        <div class="col-md-2">
            <input class="form-control" type="number" name="Sold[]" value="" />
        </div>
        <div class="col-md-2">
            <input step="0.01" class="form-control" type="number" name="Price[]" required />
        </div>
        <div class="col-md-1">
            <button onclick="$(this).closest('.row').remove();" class="btn btn-danger"><i class="fa fa-trash"></i></button>
        </div>
    </div>
</script>
<script>

    $('#add-ps').click(function(){
        $('#ps-row-tmpl').tmpl().appendTo('#products-wrap');
        $('#products-wrap select[name^=CategoryID]:last').select2({
            theme: 'bootstrap'
        }).on("select2:select", function(e){
            $.post('/admin/get_product_by_category', {id: e.params.data.id}, function(html){
                var row = $('#products-wrap select[name^=CategoryID]:last').closest('.row');
                row.find('.prod-input').html(html);
                row.find('.prod-input select').select2({
                    theme: 'bootstrap'
                });
            });
        });
    });

</script>