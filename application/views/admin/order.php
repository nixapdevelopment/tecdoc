<div class="container">
    <h1><?= lang('Order') ?> # <?= $order->OrderID ?></h1>
    <div>
        <a class="btn btn-info" href="<?= site_url('admin/orders', $_GET) ?>"><i class="glyphicon glyphicon-chevron-left"></i> <?= lang('BackToList') ?></a>
        <?php if (!empty($order->OrderID)) { ?>
        <a class="btn btn-warning" href="<?= site_url('admin/edit_order/' . $order->OrderID) ?>"><i class="glyphicon glyphicon-pencil"></i> Redacteza</a>
        <?php } ?>
    </div>
    <hr />
    <div class="row">
        <div class="col-md-3">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><?= lang('OrderDelails') ?></h3>
                </div>
                <div class="panel-body">
                    <p>ID: # <?= $order->OrderID ?></p>
                    <p><?= lang('Date') ?>: <?= date('d.m.Y - H:i:s', strtotime($order->Date)) ?></p>
                    <p><?= lang('PaymentType') ?>: <?= $order->PaymentType ?></p>
                    <p><?= lang('ClientType') ?>: <?= $order->Type ?></p>
                    <p><?= lang('Status') ?>: <?= $order->Status ?></p>
                    <p><strong><?= lang('Amount') ?>: <?= $order->Amount ?> MDL</strong></p>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><?= lang('UserInfo') ?></h3>
                </div>
                <div class="panel-body">
                    <p><?= lang('Name') ?>: <?= empty($order->UserID) ? $order->Name : ('<a target="_blank" href="' . site_url('admin/user/' . $order->UserID) . '">' . $order->Name . '</a>') ?></p>
                    <p><?= lang('Email') ?>: <?= $order->Email ?></p>
                    <p><?= lang('Phone') ?>: <?= $order->Phone ?></p>
                    <p><?= lang('Region') ?>: <?= $this->config->item($order->Region, 'regions') ?></p>
                    <p><?= lang('Address') ?>: <?= $order->Address ?></p>
                    <p><?= lang('Zip') ?>: <?= $order->Zip ?></p>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><?= lang('OrderProducts') ?></h3>
                </div>
                <div class="panel-body">
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th><?= lang('SKU') ?></th>
                                <th><?= lang('ProductName') ?></th>
                                <th><?= lang('Quantity') ?></th>
                                <th><?= lang('Price') ?></th>
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($order_products as $product) { ?>
                            <tr>
                                <td><?= $product->Sku ?></td>
                                <td><?= $product->Name ?></td>
                                <td><?= $product->Quantity ?></td>
                                <td><?= $product->Price ?></td>
                                <td><?= number_format($product->Price * $product->Quantity, 2) ?></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <div class="text-right">
                        <h3>Total: <?= $order->Amount ?> MDL</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>