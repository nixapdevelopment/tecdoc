
<div class="container">

    <div class="in-admin">
        <div class="row">
            <div class="col-md-6">
                <h3 class="upper-case-title">Categorii</h3>
            </div>
            <div class="col-md-6 text-right">
                <a id="add-category-btn" href="#" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> Adugă categorie</a>
            </div>
        </div>
    </div>
    <div class="">
        <div class="col-md-5" id="tree-wrap">
            <div class="easy-tree">
                <?= $tree ?>
            </div>
        </div>
        <div class="col-md-7">
            <form id="edit-category-form" enctype="multipart/form-data" method="post">
                
            </form>
        </div>
    </div>
</div>

<script>
    (function ($) {
        
        $("#edit-category-form").validate({
            errorClass: "text-danger",
            validClass: "text-success",
            errorElement: "span",
            focusCleanup: false,
            focusInvalid: true,
            onsubmit: true,
            ignore: "",
            submitHandler: function(form, event)
            {
                var form = event.currentTarget;
                var formData = new FormData(form);

                var xhr = new XMLHttpRequest();
                xhr.open("POST", "<?= site_url('admin/save_category') ?>");

                xhr.onreadystatechange = function()
                {
                    if (xhr.readyState == 4)
                    {
                        if(xhr.status == 200)
                        {
                            notif({
                                msg: "<?= lang('CategorySaved') ?>",
                                type: "success",
                                position: "right"
                            });
                            
                            var result = $.parseJSON(xhr.responseText);
                            
                            getTree(result.categoryId);
                            
                            getCategoryEditForm(result.categoryId, result.parentId);
                        }
                    }
                };
                xhr.send(formData);
            },
            invalidHandler: function ()
            {
                notif({
                    msg: "<?= lang('InvalidForm') ?>",
                    type: "error",
                    position: "right"
                });
            }
        });
        
        function init()
        {
            $('.easy-tree').EasyTree({
                i18n: {}
            });
        }
        window.onload = init();
        
        $('#tree-wrap').on('click', '.edit-item', function(){
            var categoryID = $(this).closest('li').attr('cat-id');
            var parentID = $(this).closest('li').attr('parent-id');
            
            getCategoryEditForm(categoryID, parentID);
        });
        
        $('#tree-wrap').on('click', '.delete-item', function(){
            var categoryID = $(this).closest('li').attr('cat-id');
            deleteCategory(categoryID);
        });
        
        $('#tree-wrap').on('click', '.add-item', function(){
            var parentID = $(this).closest('li').attr('cat-id');
            getCategoryEditForm(0, parentID);
        });
        
        function getCategoryEditForm(id, parentId)
        {
            $('#edit-category-form').html(LOADER);
            $.post('<?= site_url('admin/get_category_form') ?>', {categoryID: id, parentID: parentId}, function(html){
                $('#edit-category-form').html(html);
            });
        }
        
        function deleteCategory(id)
        {
            if (confirm('<?= lang('ConfirmCategoryDelete') ?>'))
            {
                $.post('<?= site_url('admin/delete_category') ?>', {categoryID: id}, function(){
                    $('.easy-tree li[cat-id=' + id + ']').remove();
                });
                
                notif({
                    msg: "<?= lang('CategoryDeleted') ?>",
                    type: "success",
                    position: "right"
                });
                
                getCategoryEditForm(0, 0);
            }
        }
        
        $('#add-category-btn').click(function(e){
            e.preventDefault();
            getCategoryEditForm(0, 0);
        });
        
        function getTree(categoryId)
        {
            $.post('<?= site_url('admin/categories') ?>', {}, function(html){
                $('#tree-wrap').html('<div class="easy-tree">' + html + '</div>');
                init();
                
                var li = $('.easy-tree li[cat-id=' + categoryId +']');
                
                li.parents('li[cat-id]').each(function(){
                    $(this).find('>span>span.glyphicon').click();
                });
            });
        }
        
    })(jQuery)
</script>