<div class="container">
    <div class="dash-block">
        <h3><?= lang('Orders') ?></h3>
        <div class="row">
            <div class="col-md-12">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th class="text-center">ID</th>
                            <th><?= lang('Name') ?></th>
                            <th><?= lang('Date') ?></th>
                            <th><?= lang('Address') ?></th>
                            <th><?= lang('Amount') ?></th>
                            <th><?= lang('PaymentType') ?></th>
                            <th><?= lang('Status') ?></th>
                            <th style="width: 220px;" class="text-center"></th>                            
                        </tr>
                    </thead>
                    <tbody>
                        <form>
                            <tr>
                                <td></td>
                                <td>
                                    <?= form_dropdown('ClientID', $clients, $this->input->get('ClientID'), 'class="form-control"') ?>
                                </td>
                                <td>
                                    <div style="max-width: 170px;" class="input-group">
                                        <input value="<?= $this->input->get('Date') ?>" onchange="return false;" type="text" name="Date" class="form-control datepicker" />
                                        <span class="input-group-btn">
                                            <button onclick="$('input[name=Date]').val('')" style="margin-top: 1px;" class="btn btn-danger" type="button">Clear</button>
                                        </span>
                                    </div>
                                </td>
                                <td></td>
                                <td></td>
                                <td>
                                    <?= form_dropdown('PaymentType', ['' => 'All', 'Cash' => 'Cash', 'Card' => 'Card'], $this->input->get('PaymentType'), 'class="form-control select-2"') ?>
                                </td>
                                <td>
                                    <?= form_dropdown('Status', ['' => 'All', 'Pending' => 'Pending', 'Paid' => 'Paid'], $this->input->get('Status'), 'class="form-control select-2"') ?>
                                </td>
                                <td>
                                    <button type="submit" class="btn btn-primary"><i class="fa fa-filter"></i> <?= lang('Filter') ?></button>
                                    <a href="<?= site_url('admin/orders') ?>" type="submit" class="btn btn-danger"><?= lang('ClearFilters') ?></a>
                                </td>
                            </tr>
                        </form>
                        <?php foreach ($orders as $order) { ?>
                        <tr>
                            <td class="text-center"><a href="<?= site_url('admin/order/' . $order->OrderID) ?>"><?= $order->OrderID ?></a></td>
                            <td><a target="<?= empty($order->UserID) ? '' : '_blank' ?>" style="<?= empty($order->UserID) ? 'color:inherit;' : '' ?>" href="<?= empty($order->UserID) ? '#' : site_url('admin/user/' . $order->UserID) ?>"><?= $order->Name ?></a></td>
                            <td><?= date('d.m.Y', strtotime($order->Date)) ?></td>
                            <td><?= $order->Address ?></td>
                            <td><?= $order->Amount ?></td>
                            <td><?= $order->PaymentType ?></td>
                            <td><?= $order->Status ?></td>
                            <td class="text-center">
                                <a href="<?= site_url('admin/order/' . $order->OrderID, $_GET) ?>" class="btn btn-xs btn-success"><i class="fa fa-eye"></i></a>
                                <?php if ($order->IsNew) { ?>
                                <span class="label label-primary">NEW</span>
                                <?php } ?>
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
                <div class="text-center">
                    <?= $pagination ?>
                </div>
            </div>
        </div>
    </div>
</div>