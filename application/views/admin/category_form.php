<?php

$active_langs = $this->config->item('languages');

?>

<div>
    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <?php foreach ($active_langs as $langID => $lang) { ?>
        <li role="presentation" class="<?= $langID == 1 ? 'active' : '' ?>"><a href="#tab-<?= $lang['LangFile'] ?>" aria-controls="tab-<?= $lang['LangFile'] ?>" role="tab" data-toggle="tab"><?= $lang['Name'] ?></a></li>
        <?php } ?>
        <li role="presentation"><a href="#tab-main" aria-controls="tab-main" role="tab" data-toggle="tab">Home</a></li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <br />
        <?php foreach ($active_langs as $langID => $lang) { ?>
        <div role="tabpanel" class="tab-pane <?= $langID == 1 ? 'active' : '' ?>" id="tab-<?= $lang['LangFile'] ?>">
            <div class="form-group">
                <label class="control-label"><?= lang('CategoryName') ?> <i class="text-danger">*</i></label>
                <input required class="form-control" type="text" name="Name[<?= $langID ?>]" value="<?= isset($category_langs[$langID]->Name) ? $category_langs[$langID]->Name : '' ?>" />
            </div>
            <div class="form-group">
                <label class="control-label"><?= lang('CategoryTitle') ?> <i class="text-danger">*</i></label>
                <input required class="form-control" type="text" name="Title[<?= $langID ?>]" value="<?= isset($category_langs[$langID]->Title) ? $category_langs[$langID]->Title : '' ?>" />
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label"><?= lang('Keywords') ?></label>
                        <textarea class="form-control" name="Keywords[<?= $langID ?>]"><?= isset($category_langs[$langID]->Keywords) ? $category_langs[$langID]->Keywords : '' ?></textarea>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label"><?= lang('Description') ?></label>
                        <textarea class="form-control" name="Description[<?= $langID ?>]"><?= isset($category_langs[$langID]->Description) ? $category_langs[$langID]->Description : '' ?></textarea>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>
        <div role="tabpanel" class="tab-pane" id="tab-main">
            <div class="form-group">
                <label class="control-label"><?= lang('CategoryParent') ?></label>
                <?= form_dropdown('ParentID', $all_categories, isset($category->ParentID) ? $category->ParentID : $parentID, 'class="form-control"') ?>
            </div>
            <div class="form-group">
                <label class="control-label"><?= lang('CategoryLink') ?></label>
                <div class="input-group">
                    <div class="input-group-addon"><?= site_url() ?></div>
                    <input class="form-control" type="text" name="Link" value="<?= $category_link ?>" />
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label"><?= lang('ShowIn') ?></label>
                        <select class="form-control" name="Type">
                            <option value="All"><?= lang('ShowInAll') ?></option>
                            <option value="Retail"><?= lang('ShowInRetail') ?></option>
                            <option value="Angro"><?= lang('ShowInAngro') ?></option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label"><?= lang('CatStatus') ?></label>
                        <select class="form-control" name="Status">
                            <option value="Active"><?= lang('CatStatusActive') ?></option>
                            <option value="Disabled"><?= lang('CatStatusDisabled') ?></option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label"><?= lang('CategoryImage') ?> <?= empty($category->Image) ? '<i class="text-danger">*</i>' : '' ?></label>
                        <input accept=".png,.jpg" <?= empty($category->Image) ? 'required' : '' ?> class="form-control" type="file" name="Image" />
                    </div>
                </div>
                <div class="col-md-6">
                    <?php if (!empty($category->Image)) { ?>
                    <img style="" src="<?= base_url('public/uploads/categories/' . $category->Image) ?>" class="img-thumbnail" />
                    <?php } ?>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
<br />
<div class="text-center">
    <input type="hidden" name="CategoryID" value="<?= $categoryID ?>" />
    <button type="submit" class="btn btn-success btn-lg"><i class="fa fa-floppy-o"></i> <?= lang('Save') ?></button>
</div>