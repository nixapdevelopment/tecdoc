<?php
$active_langs = $this->config->item('languages');
?>
<div class="container">
    <div class="in-admin">
        <div class="row">
            <div class="col-md-6">
                <h3 class="upper-case-title"><?= lang('Pages') ?></h3>
            </div>
            <div class="col-md-6 text-right">
                <a id="add-filter-btn" href="<?= site_url('admin/edit_page') ?>" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> <?= lang('AddPage') ?></a>
            </div>
        </div>
    </div>
    <div>
        <form id="edit-page-form" method="post" enctype="multipart/form-data">
            <?= $this->session->flashdata('success') ?>
            <div>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#tab-main" aria-controls="tab-main" role="tab" data-toggle="tab"><?= lang('GeneralData') ?></a></li>
                    <li role="presentation"><a href="#tab-images" aria-controls="tab-images" role="tab" data-toggle="tab"><?= lang('Images') ?></a></li>
                </ul>
                
                <div class="tab-content">
                    <br />
                    <div role="tabpanel" class="tab-pane active" id="tab-main">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label"><?= lang('Link') ?></label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><?= site_url() ?></span>
                                        <?php if (!empty($article->ID) && $article->ID == 1) { ?>
                                        <input name="Link" value="" type="text" class="form-control" readonly />
                                        <?php } else { ?>
                                        <input name="Link" value="<?= isset($article_link) ? $article_link : '' ?>" type="text" class="form-control" />
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label"><?= lang('Status') ?></label>
                                    <select <?= !empty($article->ID) && $article->ID == 1 ? 'disabled' : '' ?> class="form-control" name="Status">
                                        <option <?= @$article->Status == 'Active' ? 'selected' : '' ?> value="Active"><?= lang('ProdStatusActive') ?></option>
                                        <option <?= @$article->Status == 'Disabled' ? 'selected' : '' ?> value="Disabled"><?= lang('ProdStatusDisabled') ?></option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div>
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <?php foreach ($active_langs as $langID => $lang) { ?>
                                    <li role="presentation" class="<?= $langID == 1 ? 'active' : '' ?>"><a href="#tab-<?= $lang['LangFile'] ?>" aria-controls="tab-<?= $lang['LangFile'] ?>" role="tab" data-toggle="tab"><?= $lang['Name'] ?></a></li>
                                <?php } ?>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <br />
                                <?php foreach ($active_langs as $langID => $lang) { ?>
                                    <div role="tabpanel" class="tab-pane <?= $langID == 1 ? 'active' : '' ?>" id="tab-<?= $lang['LangFile'] ?>">
                                        <div class="form-group">
                                            <label class="control-label"><?= lang('Title') ?> <i class="text-danger">*</i></label>
                                            <input required class="form-control" type="text" name="Title[<?= $langID ?>]" value="<?= isset($article_langs[$langID]->Title) ? $article_langs[$langID]->Title : '' ?>" />
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label"><?= lang('Text') ?></label>
                                            <textarea class="form-control ckeditor" name="Text[<?= $langID ?>]"><?= isset($article_langs[$langID]->Text) ? $article_langs[$langID]->Text : '' ?></textarea>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label"><?= lang('Keywords') ?></label>
                                                    <textarea class="form-control" name="Keywords[<?= $langID ?>]"><?= isset($article_langs[$langID]->Keywords) ? $article_langs[$langID]->Keywords : '' ?></textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label"><?= lang('Description') ?></label>
                                                    <textarea class="form-control" name="Description[<?= $langID ?>]"><?= isset($article_langs[$langID]->Description) ? $article_langs[$langID]->Description : '' ?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="tab-images">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label"><?= lang('Images') ?></label>
                                    <input class="form-control" accept=".png,.jpg" type="file" name="Images[]" multiple />
                                </div>
                                <table id="product-images" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th class="text-center">№</th>
                                            <th>Poza</th>
                                            <th>Poza generala</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if (count($images) > 0) { ?>
                                        <?php $i = 0; ?>
                                        <?php foreach ($images as $img) { ?>
                                        <?php $i++; ?>
                                        <tr data-image-id="<?= $img['ID'] ?>">
                                            <td class="text-center">
                                                <?= $i ?>
                                            </td>
                                            <td class="text-center">
                                                <a rel="photos" href="<?= base_url('public/uploads/pages/' . $img['Image']) ?>" class="fancybox">
                                                    <img style="height: 90px;" src="<?= base_url('public/uploads/pages/' . $img['Thumb']) ?>" class="img-thumbnail" />
                                                </a>
                                            </td>
                                            <td class="text-center">
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" name="PrimaryImage" <?= $img['IsMain'] == 1 ? 'checked' : '' ?> value="<?= $img['ID'] ?>">
                                                    </label>
                                                </div>
                                            </td>
                                            <td class="text-center">
                                                <?php if ($img['IsMain'] != 1) { ?>
                                                <a href="#" onclick="return deleteImage(<?= $img['ID'] ?>)" class="text-danger"><i class="glyphicon glyphicon-trash"></i> Delete</a>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                        <?php } else { ?>
                                        <tr>
                                            <td colspan="4"class="text-center">
                                                Nici o poza încarcată
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label"><?= lang('Files') ?></label>
                                    <input class="form-control" accept=".doc,.docx,.rar,.zip,.pdf,.xls,.xlsx" type="file" name="Files[]" multiple />
                                </div>
                                <table id="product-files" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th class="text-center">№</th>
                                            <th><?= lang('FileName') ?></th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if (count($files) > 0) { ?>
                                        <?php $i = 0; ?>
                                        <?php foreach ($files as $file) { ?>
                                        <?php $i++; ?>
                                        <tr data-file-id="<?= $file->ID ?>">
                                            <td class="text-center">
                                                <?= $i ?>
                                            </td>
                                            <td class="text-center">
                                                <a href="<?= base_url('public/uploads/pages/' . $file->Path) ?>" target="_blank">
                                                    <?= $file->Name ?>
                                                </a>
                                            </td>
                                            <td class="text-center">
                                                <a href="#" onclick="return deleteFile(<?= $file->ID ?>)" class="text-danger"><i class="glyphicon glyphicon-trash"></i> Delete</a>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                        <?php } else { ?>
                                        <tr>
                                            <td colspan="3"class="text-center">
                                                Nici un fisier încarcat
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
            
            <div>
                <button type="submit" class="btn btn-success btn-lg"><i class="fa fa-floppy-o"></i> <?= lang('Save') ?></button>
            </div>
        </form>
    </div>
</div>

<script>

    $('input.datepicker').datepicker({
        format: 'dd.mm.yyyy',
        autoclose: true
    });
    
    $("#edit-page-form").validate({
        errorClass: "text-danger",
        validClass: "text-success",
        errorElement: "span",
        focusCleanup: false,
        focusInvalid: true,
        onsubmit: true,
        ignore: "",
        invalidHandler: function ()
        {
            notif({
                msg: "<?= lang('InvalidForm') ?>",
                type: "error",
                position: "right"
            });
        }
    });
    
    $('a.fancybox').fancybox();
    
    function deleteFile(fileID)
    {
        if (confirm('Confirmați?'))
        {
            $.post('<?= site_url('admin/delete_file') ?>', {fileID: fileID}, function(){
                $('tr[data-file-id=' + fileID + ']').remove();
            });
        }
        return false;
    }
    
    function deleteImage(imgID)
    {
        if (confirm('Confirmați?'))
        {
            $.post('<?= site_url('admin/delete_page_image') ?>', {imgID: imgID}, function(){
                $('tr[data-image-id=' + imgID + ']').remove();
            });
        }
        return false;
    }

</script>