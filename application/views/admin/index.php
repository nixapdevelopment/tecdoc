<div class="container">
    <div class="row">
        <div class="col-md-4">
            <a href="<?= site_url('admin/products') ?>">
                <div class="dash-item text-center">
                    <i class="fa fa-shopping-bag"></i>
                    <h4><?= $product_count ?> - продуктов (без TecDoc)</h4>
                </div>
            </a>
        </div>
        <div class="col-md-4">
            <a href="<?= site_url('admin/categories') ?>">
                <div class="dash-item text-center">
                    <i class="fa fa-list-ul"></i>
                    <h4><?= $category_count ?> - категории</h4>
                </div>
            </a>
        </div>
        <div class="col-md-4">
            <a href="<?= site_url('admin/users/clients') ?>">
                <div class="dash-item text-center">
                    <i class="fa fa-users"></i>
                    <h4><?= $client_count ?> - клиенты</h4>
                </div>
            </a>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="dash-block">
        <h3>Последние онлайн заказы</h3>
        <div class="row">
            <div class="col-md-12">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th class="text-center">ID</th>
                            <th><?= lang('Name') ?></th>
                            <th><?= lang('Date') ?></th>
                            <th><?= lang('Address') ?></th>
                            <th><?= lang('Amount') ?></th>
                            <th><?= lang('PaymentType') ?></th>
                            <th><?= lang('ClientType') ?></th>
                            <th><?= lang('Status') ?></th>
                            <th class="text-center">Посмотреть</th>                            
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($orders as $order) { ?>
                        <tr>
                            <td class="text-center"><a href="<?= site_url('admin/order/' . $order->OrderID) ?>"><?= $order->OrderID ?></a></td>
                            <td><a style="<?= empty($order->UserID) ? 'color:inherit;' : '' ?>" href="<?= empty($order->UserID) ? '#' : site_url('admin/user/' . $order->UserID) ?>"><?= $order->Name ?></a></td>
                            <td><?= $order->Date ?></td>
                            <td><?= $order->Address ?></td>
                            <td><?= $order->Amount ?></td>
                            <td><?= $order->PaymentType ?></td>
                            <td><?= $order->Type ?></td>
                            <td><?= $order->Status ?></td>
                            <td class="text-center">
                                <a href="<?= site_url('admin/order/' . $order->OrderID) ?>" class="btn btn-xs btn-success"><i class="fa fa-eye"></i></a>
                                <?php if ($order->IsNew) { ?>
                                <span class="label label-primary">Новый</span>
                                <?php } ?>
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
                <div class="text-center">
                    <a href="<?= site_url('admin/orders') ?>" class="btn btn-info"><i class="fa fa-shopping-cart"></i> Все заказы</a>
                </div>
            </div>
        </div>
    </div>
</div>
