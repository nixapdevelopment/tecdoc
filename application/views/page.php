<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<section class="content">
    <div class="container">
        <h1><?= $page->Title ?></h1>
        <br />
        <div>
            <?= $page->Text ?>
        </div>
    </div>
</section>