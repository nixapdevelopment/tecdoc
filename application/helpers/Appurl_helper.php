<?php

if (!function_exists('site_url'))
{
    function site_url($uri = '', $get = [], $combine_get = false, $protocol = NULL)
    {
        $ar = explode('/', $_SERVER['REQUEST_URI']);
        $ar = array_filter($ar);
        
        $langs = get_instance()->config->item('languages');
        
        if (isset($ar[1]))
        {
            foreach ($langs as $langID => $lang)
            {
                if ($ar[1] == $lang['Slug'])
                {
                    $uri = $lang['Slug'] . '/' . $uri;
                    break;
                }
            }
        }
        
        $url = get_instance()->config->site_url($uri, $protocol);
        $segments = explode('?', $url);
        
        if (count($segments) > 1)
        {
            throw new ErrorException('Set get params as second parametr in site_url()');
        }
        
        $query_string = '';
        if (count($get) > 0)
        {
            if ($combine_get)
            {
                $get = array_merge($_GET, $get);
            }
            
            foreach ($get as $key => $val)
            {
                if ($val == '')
                {
                    unset($get[$key]);
                }
            }
            
            $arr = [];
            foreach ($get as $key => $val)
            {
                if (is_array($val))
                {
                    foreach ($val as $k => $v)
                    {
                        $arr[] = $key . '[' . $k . ']=' . $v;
                    }
                }
                else
                {
                    $arr[] = $key . '=' . $val;
                }
            }

            $query_string = '?' . implode('&', $arr);
        }
        
        return $url . $query_string;
    }
}

if (!function_exists('str_to_url'))
{
    function str_to_url($str, $delimiter = '-')
    {
        $replace = [
            'ă' => 'a',
            'î' => 'i',
            'â' => 'a',
            'ț' => 't',
            'ș' => 's',
            'Ă' => 'a',
            'Î' => 'i',
            'Â' => 'a',
            'Ț' => 't',
            'Ș' => 's',
        ];
        
        $str = str_replace(array_keys($replace), array_values($replace), $str);

        $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
        $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
        $clean = strtolower(trim($clean, '-'));
        $clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);

        return $clean;
    }
}

if (!function_exists('breadscrumbs'))
{
    function breadscrumbs($data = [])
    {
        if (empty($data))
        {
            return '';
        }
        
        $return = '<ul class="bread">';
        foreach ($data as $link => $name)
        {
            if ($link == site_url())
            {
                $return .= '<li><a href="/"><i class="fa fa-home"></i> AGM Prima pagină</a></li>';
            }
            else
            {
                if (empty($link))
                {
                    $return .= '<li>' . $name . '</li>';
                }
                else
                {
                    $return .= '<li><a href="' . $link . '">' . $name . '</a></li>';
                }
            }
        }
        
        return $return . '</ul>';
    }
}

if (!function_exists('menu'))
{
    function menu($menu_data)
    {
        $return = '<ul>';
        foreach ($menu_data as $link => $label)
        {
            $return .= '<li><a href="' . site_url($link) . '">' . $label . '</a></li>';
        }
        $return .= '</ul>';
        
        return $return;
    }
}