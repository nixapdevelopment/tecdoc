<?php defined('BASEPATH') OR exit('No direct script access allowed');

class AppController extends CI_Controller
{
    
    public $header = null;
    
    public $footer = null;
    
    public $langID = 2;
    
    public $breadscrumbs = [];
    
    public $data = [];


    public function __construct()
    {
        parent::__construct();
       
        $this->parse_lang();
    }
    
    public function parse_lang()
    {
        $ar = explode('/', $_SERVER['REQUEST_URI']);
        $ar = array_filter($ar);

        $langs = get_instance()->config->item('languages');

        if (isset($ar[1]))
        {
            foreach ($langs as $langID => $lang)
            {
                if ($ar[1] == $lang['Slug'])
                {
                    $this->langID = $langID;
                    $this->config->set_item('language', $lang['LangFile']);
                    $this->lang->load('common', $lang['LangFile']);
                    break;
                }
                else
                {
                    $this->lang->load('common', 'russian');
                }
            }
        }
        else
        {
            $this->langID = 2;
            $this->config->set_item('language', 'russian');
            $this->lang->load('common', 'russian');
        }
    }

    public function render($view, $data = [])
    {
        $this->load->view($this->header);
        $this->load->view($view, $data);
        $this->load->view($this->footer);
    }
    
    public function addBreadscrumb($link, $text)
    {
        $this->breadscrumbs[$link] = $text;
    }
    
}

class FrontController extends AppController
{
    
    public $header = 'layouts/header';
    
    public $footer = 'layouts/footer';
    
    public $_user = null;
    
    public $shopType = 'All';
    
    public $menu;
    
    public $menuType = 'Front';
    
    public $accountLink = '';
    
    public $cartAmount = 0.00;


    public function menu()
    {
        $this->menu = [
            'Admin' => [
                'admin/index' => lang('Dashboard'),
                'admin/categories' => lang('Categories'),
                'admin/products' => lang('Products'),
                'admin/filters' => lang('Filters'),
                'admin/users' => lang('Users'),
                'admin/news' => lang('News'),
                'admin/pages' => lang('Pages'),
                'admin/orders' => lang('Orders'),
                'admin/stocks' => lang('Stocks'),
            ],
            'User' => [
                'user/index' => lang('Dashboard'),
                'user/orders' => lang('Orders'),
                'user/settings' => lang('Settings'),
            ],
            'Front' => [
                '' => lang('HomePage'),
                'news' => lang('News'),
                'service' => lang('Service'),
                'despre-noi' => lang('AboutUs'),
                'contacte' => lang('Contacts'),
            ],
            'Manager' => [

            ]
        ];
    }

    public function __construct()
    {
        parent::__construct();
        
        $loged_user_id = $this->session->userdata('UserID');
        
        if ($loged_user_id)
        {
            $this->load->model('UserModel');
            $this->_user = $this->UserModel->getByID($loged_user_id);
            $this->shopType = $this->_user->Type;
            
            switch ($this->_user->Type)
            {
                case 'Admin':
                    $this->accountLink = 'admin';
                    break;
                case 'Manager':
                    $this->accountLink = 'admin';
                    break;
                case 'Angro':
                    $this->accountLink = 'user';
                    break;
                case 'Retail':
                    $this->accountLink = 'user';
                    break;
                default:
                    $this->accountLink = '';
                    break;
            }
        }
        
        $this->addBreadscrumb(site_url(), lang('HomePage'));
        
        $this->menu();
    }
    
}

class AdminController extends FrontController
{
    
    public $header = 'layouts/header';
    
    public $footer = 'layouts/footer';
    
    public function __construct()
    {
        parent::__construct();
        
        if (empty($this->_user) || $this->_user->Type != 'Admin')
        {
            redirect('login');
        }
    }
    
}

class UserController extends FrontController
{
    
    public function __construct()
    {
        parent::__construct();
        
        if (empty($this->_user))
        {
            redirect('login');
        }
        
        if ($this->_user->Status == 'NotConfirmed')
        {
            redirect('wait-confirmation');
        }
    }
    
}